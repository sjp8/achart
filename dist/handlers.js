"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const moment = require("moment");
const PubSub = require("@google-cloud/pubsub");
const ephemerisDatabase_1 = require("./logic/ephemerisDatabase");
const _1 = require("./logic/astrology/");
const event_schema_1 = require("./schema/event.schema");
const ephemeris_1 = require("./logic/astrology/ephemeris");
const ephemerisDatabase = new ephemerisDatabase_1.EphemerisDatabase();
ephemerisDatabase.connect();
function cors(res) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
}
/** Returns basic information about the app. */
function information(req, res) {
    cors(res);
    res.json({
        app: 'Real-Time Astrology',
        endpoints: Object.keys(module.exports)
    });
}
exports.information = information;
/**
 * Gets daily or hourly planetary positions for the given time span.
 *
 * @param{String} from A standard ISO-8601 date string for the start date.
 * @param{String} to A standard ISO-8601 date string for the end date.
 * @param{String} step The interval between planetary positions (day or hour).
 */
function ephemeris(req, res) {
    cors(res);
    const fromDate = moment.utc(req.query.from).startOf('day');
    const toDate = moment.utc(req.query.to).endOf('day');
    const availableSteps = ['day', 'hour', 'minute', 'week', 'month', 'year'];
    const includePlanets = req.query.planets ? req.query.planets.split(",") : null;
    const step = req.query.step && availableSteps.indexOf(req.query.step) !== -1 ? req.query.step : availableSteps[0];
    if (fromDate.isValid && toDate.isValid && step && fromDate.isBefore(toDate)) {
        const positions = ephemeris_1.ephemeris.getPlanetPositionsOverTime(fromDate, toDate, moment.duration(1, step), includePlanets);
        res.json(positions);
    }
    else {
        // parameters not correct
        res.status(404).json({
            route: 'ephemeris',
            error: `fromDate and toDate are required parameters, and must both be valid ISO 8601-compatible date format strings. step, if provided, must be one of "day" or "hour".`
        });
    }
}
exports.ephemeris = ephemeris;
/**
 * Gets an astrological event by mongo ID.
 *
 * @param{String} The mongo ID of the event whose information to retrieve.
 */
function event(req, res) {
    cors(res);
    const eventId = req.query.id || req.query.eventId || req.params.eventId;
    const type = req.query.type || req.params.type;
    ephemerisDatabase.getChangeById(eventId, type)
        .then(event => res.json({ event, eventId, type, ephemeris: ephemeris_1.ephemeris.getPlanetsArray }))
        .catch(err => res.status(404).json({
        route: 'event',
        error: 'Error retrieving event.'
    }));
}
exports.event = event;
/**
 * Gets astrological events for the given time range.
 *
 * @param{String} from The beginning of the time span in ISO 8601 compatible format.
 * @param{String} to The end of the time span in ISO 8601 compatible format.
 */
function events(req, res) {
    return __awaiter(this, void 0, void 0, function* () {
        cors(res);
        const fromDate = moment.utc(req.query.from).startOf('day');
        const toDate = moment.utc(req.query.to).endOf('day');
        const type = req.params.type || req.query.type;
        const limitCount = parseInt(req.query.limit) ? Math.min(100, Math.max(Number(req.query.limit), 1)) : 50;
        const includeOrbs = [true, 1, '1', 'true', 't', 'y', 'yes'].indexOf(req.params.includeOrbs || req.query.includeOrbs) !== -1;
        if (!(fromDate.isValid() && toDate.isValid() && fromDate.isBefore(toDate))) {
            return res.status(404).json({
                error: `fromDate and toDate must be valid ISO 8601-compatible date strings. fromDate must be before toDate.`
            });
        }
        try {
            let changes = yield ephemerisDatabase.getChanges(fromDate, toDate, type);
            // apply include orbs argument
            changes = changes.filter(event => includeOrbs ? true : (event.type != 'aspect' || event.exact));
            // add ephemeris data to each event
            changes.forEach(event => {
                event.ephemeris = ephemeris_1.ephemeris.getPlanetsForResponse(moment.utc(event.date));
            });
            res.json(changes);
        }
        catch (err) {
            res.status(400).json({
                error: 'Error querying changes from database',
                type: 'database',
                err
            });
        }
    });
}
exports.events = events;
function natal_events(req, res) {
    return __awaiter(this, void 0, void 0, function* () {
        cors(res);
        const fromDate = moment.utc(req.query.from).startOf('day');
        const toDate = moment.utc(req.query.to).endOf('day');
        const birthDate = moment.utc(req.query.birthDate);
        const latitude = Number(req.query.latitude);
        const longitude = Number(req.query.longitude);
        if (isNaN(latitude) || isNaN(longitude)) {
            return res.status(404).json({
                error: `latitude and longitude are required and must be JSON-parsable numbers.`
            });
        }
        if (!(fromDate.isValid() && toDate.isValid() && fromDate.isBefore(toDate) && birthDate.isValid())) {
            return res.status(404).json({
                error: `fromDate and toDate must be valid ISO 8601-compatible date strings. fromDate must be before toDate.`,
                from: fromDate.toISOString(),
                to: toDate.toISOString(),
                birthDate: birthDate.toISOString()
            });
        }
        try {
            const events = yield ephemerisDatabase.getNatalChanges(birthDate.unix(), latitude, longitude, fromDate, toDate);
            events.forEach(event => {
                event.ephemeris = ephemeris_1.ephemeris.getPlanetsForResponse(moment.utc(event.date));
            });
            res.json(events);
        }
        catch (err) {
            res.status(400).json({
                error: 'Error querying natal changes from database',
                type: 'database',
                obj: err
            });
        }
    });
}
exports.natal_events = natal_events;
function sequential_events(req, res) {
    cors(res);
    const eventId = req.query.eventId;
    const type = req.params.type || req.query.type;
    const countBefore = parseInt(req.query.countBefore) || 4;
    const countAfter = parseInt(req.query.countAfter) || 4;
    ephemerisDatabase.getSequentialChanges(eventId, type, countBefore, countAfter)
        .then(events => {
        events.forEach(event => {
            event.ephemeris = ephemeris_1.ephemeris.getPlanetsForResponse(moment.utc(event.date));
        });
        return events;
    })
        .then(events => res.json({ events }))
        .catch(err => res.status(400).json({ error: err }));
}
exports.sequential_events = sequential_events;
/** Gets an object with zodiac information for the given date and geographic coordinates. */
const getZodiacResponse = (date, longitude, latitude) => {
    date = moment.utc(date);
    const location = {
        longitude: longitude || -122.5452863,
        latitude: latitude || 47.6497949
    };
    const planets = ephemeris_1.ephemeris.getPlanetsArray(date);
    const planetsDisplay = planets.map(position => {
        const result = position.position;
        result.planet = position.planet;
        return result;
    });
    const planetsBefore = ephemeris_1.ephemeris.getPlanetsArray(moment.utc(date).add(-1, 'seconds'));
    const houses = ephemeris_1.ephemeris.getHouses(date, location.longitude, location.latitude, _1.House.houseTypes.placidus);
    const aspects = ephemeris_1.ephemeris.getAspects(planets, planetsBefore);
    const retrograde = _1.Body.bodies.map(planet => { return { planet: planet.name, retrograde: ephemeris_1.ephemeris.getRetrograde(date, planet.name) }; });
    const information = { display: planetsDisplay, planets, houses, aspects, retrograde };
    return information;
};
/** Get the zodiac information for the given date. */
function zodiac(req, res) {
    cors(res);
    const date = moment.utc(req.query.date);
    const latitude = Number(req.query.latitude);
    const longitude = Number(req.query.longitude);
    if (!date.isValid) {
        return res.status(404).json({
            route: 'zodiac',
            error: 'Must specify a `date` parameter, an ISO 8601-compatible date string.'
        });
    }
    if (isNaN(latitude) || isNaN(longitude)) {
        return res.status(404).json({
            route: 'zodiac',
            error: 'Must specify latitude and longitude parameters, each must be valid integer or decimal numbers from -180 to 180 (longitude) or -90 to 90 (latitude).'
        });
    }
    const data = getZodiacResponse(date, longitude, latitude);
    res.json(data);
}
exports.zodiac = zodiac;
/** HTTP handler to initiate the request to the background worker (populate_natal_background). */
function populate_natal(req, res) {
    return __awaiter(this, void 0, void 0, function* () {
        cors(res);
        const latitude = Number(req.query.latitude);
        const longitude = Number(req.query.longitude);
        const birthDate = moment(req.query.birthDate);
        if (!birthDate.isValid()) {
            return res.status(404).json({
                route: 'populate_natal',
                error: 'Must specify a `date` parameter, an ISO 8601-compatible date string.'
            });
        }
        if (isNaN(latitude) || isNaN(longitude)) {
            return res.status(404).json({
                route: 'populate_natal',
                error: 'Must specify latitude and longitude parameters, each must be valid integer or decimal numbers from -180 to 180 (longitude) or -90 to 90 (latitude).'
            });
        }
        let startDate = moment.utc().startOf('day').subtract(3, 'days');
        const searchCriteria = { latitude, longitude, birthDate: birthDate.unix() };
        const max = (yield event_schema_1.NatalChange.find(searchCriteria).sort({ date: 1 }).limit(1))[0];
        const min = !max ? null : (yield event_schema_1.NatalChange.find(searchCriteria).sort({ date: -1 }).limit(1))[0];
        // if the endpoint hasn't been called for this birthdate/latitude/longitude for 3 months, add another year's worth of data.
        if (min && moment(min.date).isBefore(startDate.clone().subtract(12, 'days'))) {
            console.log('add another year of natal data, the endpoint has not been called for at least 3 months.');
            startDate = moment(max.date).add(1, 'second');
        }
        else if (min && max) {
            return res.json({
                latitude, longitude, birthDate: birthDate.unix(), added: false
            });
        }
        const batchDuration = 4; // months
        // 01 to 07.
        // 07 to 12.
        const getBufferForBatch = (index) => {
            const arg = {
                latitude,
                longitude,
                birthDate: birthDate.toISOString(),
                startDate: startDate.clone().add(index * batchDuration, 'months').toISOString(),
                endDate: startDate.clone().add((index + 1) * batchDuration, 'months').subtract(1, 'millisecond').toISOString()
            };
            const dataBuffer = Buffer.from(JSON.stringify(arg));
            return { buffer: dataBuffer, obj: arg };
        };
        try {
            const pubsub = PubSub();
            const batches = yield Promise.all([0].map((batch) => {
                return new Promise((resolve, reject) => {
                    setTimeout(() => {
                        const buffer = getBufferForBatch(batch);
                        return pubsub
                            .topic('populate_natal_background')
                            .publisher()
                            .publish(buffer.buffer)
                            .then(id => resolve({ id, request: buffer.obj }))
                            .catch(reject);
                    }, batch * 1000);
                });
            }));
            res.json({
                latitude, longitude, birthDate: birthDate.unix(), batches
            });
        }
        catch (ex) {
            console.error('Error publishing populate_natal message: ', ex);
            res.status(400).json({
                error: true
            });
        }
    });
}
exports.populate_natal = populate_natal;
