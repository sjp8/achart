"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const moment = require("moment");
const path = require("path");
const swisseph = require("swisseph");
const _1 = require("./");
/**
 * Methods to query an ephemeris.
 */
class Ephemeris {
    constructor(dataSource, swissEphemeris) {
        /** The default log date format */
        this.dateLogFormat = 'YYYY-MM-DD h:mm:ss a';
        /** The sequence of time intervals on which to perform searches for event changes. */
        this.recursiveSearchSeries = [24 * 60 * 60, 60 * 60, 30 * 60, 10 * 60, 60, 10, 1];
        /** The minimum search unit granularity (choose seconds or minutes). */
        this.recursiveSearchUnit = 'second';
        this.dataSource = dataSource;
        this.ephemeris = swissEphemeris;
        this.load();
    }
    /** Get the data source enum value*/
    get dataSourceValue() { return Ephemeris.dataSources[this.dataSource]; }
    /** Setup the data source (if necessary). */
    load() {
        this.ephemeris.dataSource = Ephemeris.dataSources[this.dataSource];
        this.ephemeris.swe_set_ephe_path(Ephemeris.ephemerisFiles);
    }
    /** Gets the position of a planet at the given date and time. */
    getPlanet(date, planet) {
        const julian = Ephemeris.getJulianTime(date);
        const planetaryConstant = this.ephemeris['SE_' + planet.toUpperCase()];
        const body = this.ephemeris.swe_calc_ut(julian, planetaryConstant, this.dataSourceValue | this.ephemeris.SEFLG_SPEED);
        if (body.error) {
            console.error(body.error);
            return null;
        }
        return new _1.PlanetaryPosition(date, planet, body);
    }
    /** Gets the position of all the planets including Chiron (see Body.bodies), for the given date and time. */
    getPlanets(date) {
        let planets = {};
        _1.Body.bodies.forEach((body) => {
            const position = this.getPlanet(date, body.name);
            planets[body.name] = position;
        });
        return planets;
    }
    getPlanetsForResponse(date) {
        const planets = this.getPlanetsArray(date);
        const planetStrings = planets.map(p => p.planet);
        const longitudes = planets.map(p => p.longitude);
        const latitudes = planets.map(p => p.latitude);
        const longitudeSpeeds = planets.map(p => p.longitudeSpeed);
        const distances = planets.map(p => p.distance);
        return { planets: planetStrings, longitudes, latitudes, longitudeSpeeds, distances };
    }
    getPlanetsArray(date) {
        return _1.Body.bodies.map(body => this.getPlanet(date, body.name));
    }
    /** Gets the phase of the moon for the given date and time. */
    getMoonPhase(date) {
        const sun = this.getPlanet(date, 'Sun');
        const moon = this.getPlanet(date, 'Moon');
        const angle = _1.Aspect.getAngleBetweenLongitudes(sun.longitude, moon.longitude);
        // waxing if they are separating, because the larger the angle, the closer to full moon.
        const waxing = _1.PlanetaryAspect.planetsAreSeparating(sun, moon);
        return new _1.MoonPhase(date, angle, waxing, moon.longitude, moon.distance, sun.longitude);
    }
    /** Convert date parts (year, month, day, hour, minute, second) to a julian time number. */
    static getJulianTimeFromParts(parts) {
        const julian = swisseph.swe_utc_to_jd(parts.year, parts.month, parts.day, parts.hour, parts.minute, parts.second, swisseph.SE_GREG_CAL);
        if (julian.error) {
            console.error('Error in getJulianTime: ' + julian.error);
            return null;
        }
        else {
            return julian.julianDayUT;
        }
    }
    /** Get the julian time number from a date. Formerly getJulianTimeFromDate. */
    static getJulianTime(date) {
        return Ephemeris.getJulianTimeFromParts(Ephemeris.getDateParts(date));
    }
    /**
     * Get parts of a date (year, month, day, hour, minute, second) from a date.
     * Formerly getDataFromMoment.
     */
    static getDateParts(date) {
        return {
            year: date.year(),
            month: date.month() + 1,
            day: date.date(),
            hour: date.hour(),
            minute: date.minute(),
            second: date.seconds()
        };
    }
    /** Gets the planetary aspect between the two planets, using their before positions to determine the exactness of the aspect. */
    getAspect(planet1, planet2, planet1Before, planet2Before) {
        const before = new _1.PlanetaryAspect(planet1Before.date, planet1Before, planet2Before, false);
        const after = new _1.PlanetaryAspect(planet1.date, planet1, planet2, false);
        const aspect = after.aspect;
        const isExactOpposition = aspect && aspect.angle === 180 && before.applying && !after.applying;
        // const isExactConjunction = aspect && aspect.angle === 0 && PlanetaryAspect.isAtConjunction(planet1.longitude, planet1.longitudeSpeed, planet2.longitude, planet2.longitudeSpeed);
        const isExactConjunction = aspect && aspect.angle === 0 && before.applying && !after.applying;
        const exact = aspect && (_1.PlanetaryAspect.isCrossingOverAngle(before.angle, after.angle, aspect.angle) || isExactConjunction || isExactOpposition);
        return new _1.PlanetaryAspect(planet1.date, planet1, planet2, exact);
    }
    /**
     * Gets all current active aspects between all the planets, populating
     * exactness and entering/leaving status.
     */
    getAspects(planets, planetsBefore) {
        let aspects = [];
        planets.forEach((planet1, index1) => {
            planets.forEach((planet2, index2) => {
                if (planet1.planet >= planet2.planet) {
                    return; // alphabetical
                }
                const aspect = this.getAspect(planet1, planet2, planetsBefore[index1], planetsBefore[index2]);
                aspects.push(aspect);
            });
        });
        return aspects;
    }
    /** Gets all changed aspects between the transiting planets (in the past, and now) and the given static natal planets. */
    getChangedNatalAspects(planets, planetsBefore, natalPlanets) {
        const changedAspects = [];
        planets.forEach((transit, transitIndex) => {
            natalPlanets.forEach((natal, natalIndex) => {
                const afterAspect = this.getAspect(transit, natal, planetsBefore[transitIndex], natal);
                const beforeAspect = new _1.PlanetaryAspect(planetsBefore[transitIndex].date, planetsBefore[transitIndex], natal);
                const exact = afterAspect.exact;
                const newAspect = !beforeAspect.hasAspect && afterAspect.hasAspect;
                const fadedAspect = beforeAspect.hasAspect && !afterAspect.hasAspect;
                if (exact || newAspect) {
                    changedAspects.push(afterAspect);
                }
                else if (fadedAspect) {
                    changedAspects.push(beforeAspect);
                }
            });
        });
        return changedAspects;
    }
    /**
     * Gets all changed aspects (in the last second) between all the planets, using the before
     * positions of the planets to determine exactness and entering/leaving status.
     */
    getChangedAspects(planets, planetsBefore) {
        let changedAspects = [];
        // new version:
        // * goes through planets, calculates aspects (optimizes by only checking aspects that are likely... might not work
        // with recursive case, or even because things are not necessarily done in order.
        // * 
        planets.forEach((planet1, index1) => {
            planets.forEach((planet2, index2) => {
                if (planet1.planet >= planet2.planet) {
                    return; // alphabetical
                }
                const afterAspect = this.getAspect(planet1, planet2, planetsBefore[index1], planetsBefore[index2]);
                const beforeAspect = new _1.PlanetaryAspect(planetsBefore[index1].date, planetsBefore[index1], planetsBefore[index2], false);
                // we have an exact aspect (it is the closest possible angle, by the second, to the exact aspect angle).
                const exact = afterAspect.exact;
                // we have an entering aspect (no aspect before, aspect now).
                const newAspect = !beforeAspect.hasAspect && afterAspect.hasAspect;
                // we have a leaving aspect (it was there before, now it's gone).
                const fadedAspect = beforeAspect.hasAspect && !afterAspect.hasAspect;
                if (exact || newAspect) {
                    changedAspects.push(afterAspect);
                }
                else if (fadedAspect) {
                    changedAspects.push(beforeAspect);
                }
            });
        });
        return changedAspects;
    }
    /** Gets the houses for the given date, at the given location on the surface of the Earth. */
    getHouses(date, longitude, latitude, houseSystem = 'P') {
        const julian = Ephemeris.getJulianTime(date);
        console.log(`${julian}, ${latitude}, ${longitude}`);
        const houses = this.ephemeris.swe_houses(julian, latitude, longitude, houseSystem);
        if (houses.error) {
            console.error(houses.error);
            return;
        }
        let result = [];
        for (let i = 0; i < houses.house.length; i++) {
            const position = new _1.HousePosition(date, i + 1, houses.house[i], houses.house[(i + 1) % 12]);
            result.push(position);
        }
        return result;
    }
    /** Given an array of 12 houses, returns the appropriate house for the given longitude. */
    getHouseForLongitude(houses, longitude) {
        return houses.find((house, houseIndex) => longitude >= house.cusp && (longitude < house.end || house.number === 12));
    }
    /** Gets the retrograde status for the given date and planet. */
    getRetrograde(date, planet) {
        const lastSecond = moment.utc(date).add(-1, 'seconds');
        const position = this.getPlanet(date, planet);
        const positionBefore = this.getPlanet(lastSecond, planet);
        return position.longitude < positionBefore.longitude;
    }
    /**
     * A generic function for searching planetary changes over a period of time.
     *
     * @param beginDate Search from this date.
     * @param endDate Search until this date.
     * @param changeFunction Function to compute changes between two objects retrieved by `getFunction`.
     * @param getFunction Function to retrieve an object given a date.
     * @param onEventFunction Callback to call for any change event found by the search.
     * @param logFunction Callback to call for any change event found by the search, used to log the change.
     */
    searchPlanetaryChanges(beginDate, // search from this date
    endDate, // to this date
    changeFunction, // function to compute changes between two time-consecutive getFunction results
    getFunction, // function to get data for the given date (defaults to planets positions at date)
    onEventFunction, // callback function when a change has been found
    onFinishedFunction, // callback function when all changes between beginDate and endDate have been returned
    logFunction, // callback function to log a change
    options) {
        // search starts off with a wide sweep (see Ephemeris.recursiveSearchSeries)
        // honing in on time sections with changes.
        options = options || {
            minimumUnit: this.recursiveSearchUnit,
            recursiveSearchSeries: this.recursiveSearchSeries
        };
        /**
         * Gets an array of changes for the given time period.
         *
         * @param{moment.Moment} startDate The beginning of the period to check for changes.
         * @param{number} forSeconds How far from `startDate` to check for changes.
         * @param{P[]} previous The previous <P> array, computed for startDate. If null, queries <P> with `getFunction(startDate)`.
         */
        let getChangesForPeriod = (startDate, forSeconds) => {
            // calculate the end date based on start date and the duration of the period.
            const endDate = moment.utc(startDate).add(forSeconds, options.minimumUnit);
            // if previous is null, e.g. the first time this method is called, then compute it from startDate.
            const previous = getFunction(startDate) || [];
            // get the P[] for endDate (it hasn't been computed yet)
            const current = getFunction(endDate) || [];
            // perform the change function on the previous and current changes.
            const changes = changeFunction(previous, current) || [];
            return {
                changes,
                current,
                previous
            };
        };
        // calculate the number of seconds between beginDate and endDate
        const numberTimeUnits = moment.utc(endDate).diff(beginDate, options.minimumUnit);
        const date = moment.utc(beginDate);
        let recurseCount = 0;
        // track the planetary changes (or other object from `getFunction`) for use in comparing
        // current vs. previous, and detecting which statuses changed
        let previous = null;
        /**
         * Find changes, calling `onEventFunction` and `logFunction` for each.
         *
         * @param startDate Starting at this date.
         * @param depth The 0-indexed depth, corresponding to Ephemeris.planetarySearchSeries.
         * @param forSeconds The length of time to look for changes
         */
        const recurse = (startDate, depth, forSeconds) => {
            recurseCount += 1; // keep track of recurse calls to determine when recursion is finished.
            const step = options.recursiveSearchSeries[depth];
            const isLastSearchSeries = depth == options.recursiveSearchSeries.length - 1;
            for (let m = 0; m < forSeconds; m += step) {
                //console.log('processing chunk for ' + startDate.calendar() + ' for step ' + step)
                // get changes for this time series, starting at startDate, sending cached `previous`
                // PlanetaryPosition, etc array to the computation for changes.
                const chunk = getChangesForPeriod(startDate, step);
                // if there were any changes in this time period, recurse or add them as necessary
                if (chunk.changes.length) {
                    if (isLastSearchSeries) {
                        chunk.changes.forEach((change, index) => {
                            if (onEventFunction) {
                                onEventFunction(change);
                            }
                            if (logFunction) {
                                logFunction(change);
                            }
                        });
                    }
                    else {
                        // if there were changes, and there is more 
                        //console.log('recurse into more granular search step: ' + options.recursiveSearchSeries[depth + 1])
                        recurse(moment.utc(startDate.valueOf()), depth + 1, step);
                    }
                }
                // continue to the next chunk at the current depth by adding `step` to startDate and
                // processing the next chunk for changes
                startDate.add(step, options.minimumUnit);
            }
            recurseCount -= 1;
            if (recurseCount === 0) {
                onFinishedFunction();
            }
        };
        process.nextTick(() => {
            recurse(moment.utc(beginDate.valueOf()), 0, numberTimeUnits);
        });
    }
    searchMoonPhaseChanges(beginDate, endDate, onEvent, onFinished) {
        this.searchPlanetaryChanges(beginDate, endDate, (previous, current) => {
            if (previous && previous[0] && current && current[0] && current[0].phase.name != previous[0].phase.name) {
                const prev = previous[0];
                const curr = current[0];
                return [new _1.PhaseChangeEvent(current[0].date, current[0])];
            }
            else {
                return [];
            }
        }, (date) => {
            return [this.getMoonPhase(date)];
        }, (change) => onEvent(change), onFinished, (change) => console.log(change.date.calendar() + ': ' + (change.phase.phase.waxing ? ' Waxing ' : ' Waning ') + change.phase.phase.name + ' ' + change.phase.fullAngle));
    }
    searchMoonEventChanges(beginDate, endDate, onEvent, onFinished) {
        this.searchPlanetaryChanges(beginDate, endDate, (previous, current) => {
            const p = previous[0], c = current[0];
            const apogee = p.distanceSpeed > 0 && c.distanceSpeed <= 0; // was moving away from Earth, now moving back towards.
            const perigee = p.distanceSpeed <= 0 && c.distanceSpeed > 0; // was moving towards the Earth, now moving away again.
            const an = p.latitude < 0 && c.latitude >= 0; // the moon was below the eliptic, and is now above.
            const dn = c.latitude <= 0 && p.latitude > 0; // the moon was above the ecliptic, and now is below.
            if (apogee || perigee || an || dn) {
                return [new _1.MoonChangeEvent(c.date, c, perigee, apogee, an, dn)];
            }
        }, (date) => {
            return [this.getPlanet(date, 'Moon')];
        }, (change) => onEvent(change), onFinished, (change) => console.log(`${change.date.calendar()}: ${change.types.join(', ')}, latitude: ${change.moon.latitude}, distance (AU): ${change.moon.distance}, distanceSpeed: ${change.moon.distanceSpeed}`));
    }
    searchSignChanges(beginDate, endDate, onEvent, onFinished) {
        this.searchPlanetaryChanges(beginDate, endDate, (previous, current) => {
            const p = {}, c = {};
            previous.forEach(prev => prev && prev.planet ? p[prev.planet] = prev : null);
            current.forEach(curr => curr && curr.planet ? c[curr.planet] = curr : null);
            const planets = Object.keys(p);
            const result = [];
            planets.forEach((planet) => {
                if (p[planet].sign.name != c[planet].sign.name) {
                    result.push(new _1.SignChangeEvent(c[planet].date, c[planet], p[planet]));
                }
            });
            return result;
        }, (date) => {
            const planets = this.getPlanetsArray(date);
            return planets;
        }, (change) => onEvent(change), onFinished, (change) => console.log(change.date.calendar() + ': ' + change.planet.planet + ' ' + change.planet.sign.name + ' ' + change.planet.longitude));
    }
    searchAspectChanges(beginDate, endDate, onEvent, onFinished) {
        console.log('aspect changes between ' + beginDate.calendar() + ' and ' + endDate.calendar());
        this.searchPlanetaryChanges(beginDate, endDate, (previous, current) => {
            const changed = this.getChangedAspects(current, previous);
            const events = changed.map(aspect => new _1.AspectChangeEvent(aspect.date, aspect));
            return events;
        }, (date) => {
            return this.getPlanetsArray(date);
        }, (change) => onEvent(change), onFinished, (change) => console.log(`${change.date.calendar()}: ${change.aspect.planet1.planet}-${change.aspect.aspect.name}-${change.aspect.planet2.planet}-${change.aspect.exact}-${change.aspect.applying}`));
    }
    searchNatalChanges(birthDate, latitude, longitude, natalPositions, natalHouses, beginDate, endDate, onEvent, onFinished, options) {
        this.searchPlanetaryChanges(beginDate, endDate, (previous, current) => {
            const results = [];
            // house changes
            // - for each planet, find out its house beforehand, and after.
            // - for the houses that have changed, add an event
            const getPlanetsToHouseIndex = (positions, housePositions) => {
                const result = {};
                positions.forEach(p => {
                    // planet is between this house cusp, and the next. But if the next cusp is lower than this one (it crosses over 0), then
                    // the planet should be either greater than the lower, or less than the higher index house cusp.
                    result[p.planet] = housePositions.find((h, i) => {
                        const houses = [housePositions[i], housePositions[(i + 1) % 12]];
                        if (houses[0].cusp > houses[1].cusp) {
                            return p.longitude >= houses[0].cusp || p.longitude < houses[1].cusp;
                        }
                        else {
                            return houses[0].cusp <= p.longitude && houses[1].cusp > p.longitude;
                        }
                    });
                });
                return result;
            };
            const housesBefore = getPlanetsToHouseIndex(previous, natalHouses);
            const housesAfter = getPlanetsToHouseIndex(current, natalHouses);
            // const changedHouses: { date: moment.Moment, planet: string, house: number }[] = [];
            current.forEach(p => {
                if (housesBefore[p.planet] != housesAfter[p.planet]) {
                    results.push(new _1.NatalTransitEvent(p.date, 'house', birthDate, latitude, longitude, {
                        transitingPlanet: p,
                        natalHouse: housesAfter[p.planet].index + 1
                    }));
                }
            });
            // aspects between any natal and any transiting planet
            const changedAspects = this.getChangedNatalAspects(current, previous, natalPositions);
            changedAspects.forEach(aspect => {
                aspect.exact && results.push(new _1.NatalTransitEvent(aspect.date, 'aspect', birthDate, latitude, longitude, {
                    natalPlanet: aspect.planet2,
                    transitingPlanet: aspect.planet1
                }));
            });
            return results;
        }, (date) => {
            return this.getPlanetsArray(date);
        }, onEvent, onFinished, change => change.natalHouse ? console.log(`natal house change: ${change.date.calendar()}: ${change.transitingPlanet.planet} ${change.natalHouse}`) : console.log(`natal aspect change: ${change.date.calendar()}: ${change.transitingPlanet.planet} ${change.natalPlanet.planet}`));
    }
    searchRetrogradeChanges(beginDate, endDate, onEvent, onFinished) {
        this.searchPlanetaryChanges(beginDate, endDate, (previous, current) => {
            return current
                .filter((c, index) => previous[index] && c.retrograde !== previous[index].retrograde)
                .map(planet => new _1.RetrogradeChangeEvent(planet.date, planet));
        }, (date) => {
            const planets = this.getPlanetsArray(date);
            return planets;
        }, (change) => onEvent(change), onFinished, (change) => console.log(`${change.date.calendar()}: ${change.planet.planet} ${change.planet.retrograde ? ' retrograde' : ' direct'}`));
    }
    getPlanetPositionsOverTime(startDate, endDate, step, includePlanets = null) {
        // a list of planets to include
        const planetNames = includePlanets ? includePlanets.filter(p => _1.Body.bodies.find(b => b.name === p)) : _1.Body.bodies.map(b => b.name);
        const result = {
            fields: ['longitude', 'latitude', 'distance', 'longitudeSpeed', 'latitudeSpeed', 'retrograde'],
            planets: planetNames,
            times: []
        };
        for (let date = moment.utc(startDate); date.isBefore(endDate); date.add(step)) {
            const planets = planetNames.map(planet => this.getPlanet(date, planet));
            const rows = planets.map(planet => {
                return [planet.longitude, planet.latitude, planet.distance, planet.longitudeSpeed, planet.latitudeSpeed, planet.retrograde];
            });
            result.times.push({
                date: date.valueOf(),
                planets: rows
            });
        }
        return result;
    }
}
/** Available data sources from which to query astrological data. */
Ephemeris.dataSources = {
    jpl: swisseph.SEFLG_JPLEPH,
    swisseph: swisseph.SEFLG_SWIEPH
};
/** The ephemeris files directory. */
Ephemeris.ephemerisFiles = path.join(__dirname, '../../../ephemeris_files');
exports.Ephemeris = Ephemeris;
class PositionsOverTime {
}
exports.PositionsOverTime = PositionsOverTime;
exports.ephemeris = new Ephemeris(Ephemeris.dataSources.swisseph, swisseph);
