"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const moment = require("moment");
const types_1 = require("./types");
const ephemeris_1 = require("./ephemeris");
/** An abstract class to encapsulate any data coming from the ephemeris. */
class PlanetaryEphemerisData {
    constructor(date) {
        this.date = moment.utc(date);
    }
}
exports.PlanetaryEphemerisData = PlanetaryEphemerisData;
/** A moon phase event. */
class MoonPhase extends PlanetaryEphemerisData {
    constructor(date, angle, waxing, longitude, distance, sunLongitude) {
        super(date);
        this.planet = 'Moon';
        this.waxing = waxing;
        this.angle = angle;
        this.phase = types_1.Phase.getPhase(this.fullAngle);
        this.longitude = longitude;
        this.sunLongitude = sunLongitude;
        this.distance = distance;
    }
    /** The full angle is a number between 0 and 360 degrees. */
    get fullAngle() {
        return types_1.Phase.getFullAngle(this.angle, this.waxing);
    }
    /** Gets the angle parts (angle less than 180 degrees, and waxing status) from the full angle. */
    getAngle(fullAngle) {
        if (fullAngle >= 180) {
            return { angle: 360 - fullAngle, waxing: false };
        }
        else {
            return { angle: fullAngle, waxing: true };
        }
    }
}
exports.MoonPhase = MoonPhase;
/** Encapsulates the planetary position at the given time. */
class PlanetaryPosition extends PlanetaryEphemerisData {
    /** Retrograde status. */
    get retrograde() { return this.longitudeSpeed < 0; }
    /** Sign */
    get sign() {
        const signIndex = Math.floor(this.longitude / 30);
        return types_1.Sign.signs[signIndex];
    }
    get position() {
        return types_1.Sign.getSignAndDegree(this.longitude);
    }
    constructor(date, planet, data) {
        super(date);
        this.planet = planet;
        this.longitude = data.longitude;
        this.latitude = data.latitude;
        this.longitudeSpeed = data.longitudeSpeed;
        this.latitudeSpeed = data.latitudeSpeed;
        this.distance = data.distance;
        this.distanceSpeed = data.distanceSpeed;
    }
    /** Shortcut for Ephemeris.getJulianTime */
    get julianTime() { return ephemeris_1.Ephemeris.getJulianTime(this.date); }
    /** Shortcut for Ephemeris.getDateParts */
    get dateParts() { return ephemeris_1.Ephemeris.getDateParts(this.date); }
}
exports.PlanetaryPosition = PlanetaryPosition;
/** Information about an aspect formed by two planets at a particular time. */
class PlanetaryAspect extends PlanetaryEphemerisData {
    get exactIsSet() { return this.exact != null; }
    /** Whether or not this planetary aspect falls within any of the aspects and their orbs. */
    get hasAspect() {
        return this.aspect != null;
    }
    constructor(date, planet1, planet2, exact) {
        super(date);
        this.planet1 = planet1;
        this.planet2 = planet2;
        this.angle = PlanetaryAspect.getAngleBetween(planet1, planet2);
        this.aspect = types_1.Aspect.getAspectForAngleBinary(this.angle);
        this.exact = this.aspect && exact;
        this.applying = this.aspect && (this.exact ? null : PlanetaryAspect.isApplyingToAspect(this.planet1.longitude, this.planet1.longitudeSpeed, this.planet2.longitude, this.planet2.longitudeSpeed, this.aspect.angle));
        this.planetsSeparating = PlanetaryAspect.planetsAreSeparating(this.planet1, this.planet2);
    }
    /** Calculate the angle between two longitudes, between 0 and 180. */
    static getAngleBetween(planet1, planet2) {
        return types_1.Aspect.getAngleBetweenLongitudes(planet1.longitude, planet2.longitude);
    }
    /** Returns `true` if the planets are moving farther apart in the zodiac (i.e. increasing their angle in degrees), `false` otherwise. */
    static planetsAreSeparating(planet1, planet2) {
        // the planets are separating if adding on longitudeSpeeds results in a larger angle, coming together otherwise.
        // e.g. [30, 0.005] means a planet at 30 degrees (0 Taurus) moving forward, [70, 0.001] means a planet at 70 degrees, moving forward.
        const daysToSeconds = 1 / (60 * 60 * 24);
        const difference = types_1.Aspect.getAngleBetweenLongitudes(planet1.longitude - planet1.longitudeSpeed * daysToSeconds, planet2.longitude - planet2.longitudeSpeed * daysToSeconds);
        const differenceAfter = types_1.Aspect.getAngleBetweenLongitudes(planet1.longitude, planet2.longitude);
        return differenceAfter > difference; // separating if the angle difference is greater afterwards.
    }
    /** Returns `true` if the given longitudes and speeds represent planets that are moving closer to aspectAngle, and returns `false` if moving away from aspectAngle. */
    static isApplyingToAspect(longitude1, longitudeSpeed1, longitude2, longitudeSpeed2, aspectAngle) {
        // Is the difference between planetary angle and aspect angle increasing (separating) or decreasing (applying)?
        const secondsPerDay = 24 * 60 * 60;
        const difference = Math.abs(aspectAngle - types_1.Aspect.getAngleBetweenLongitudes(longitude1, longitude2));
        const differenceAfter = Math.abs(aspectAngle - types_1.Aspect.getAngleBetweenLongitudes(longitude1 + longitudeSpeed1 / secondsPerDay, longitude2 + longitudeSpeed2 / secondsPerDay));
        return differenceAfter < difference;
    }
    /** Returns `true` if the angle is growing closer to aspectAngle, `false` otherwise. */
    static isApplyingToAspectFromAngles(angle, angleBefore, aspectAngle) {
        const differenceBefore = Math.abs(aspectAngle - angleBefore);
        const differenceAfter = Math.abs(aspectAngle - angle);
        return differenceAfter < differenceBefore && !PlanetaryAspect.isCrossingOverAngle(angle, angleBefore, aspectAngle);
    }
    /** Returns the Aspect for the given planets (i.e. the angle between them). */
    static getAspectBetween(planet1, planet2) {
        const angle = PlanetaryAspect.getAngleBetween(planet1, planet2);
        return types_1.Aspect.getAspectForAngle(angle);
    }
    /** Returns `true` if the two planets represented by the two longitudes are at a conjunction. */
    static isAtConjunction(longitude1, longitudeSpeed1, longitude2, longitudeSpeed2) {
        const secondsPerDay = 24 * 60 * 60;
        const calculatePosition = (originalPosition, speed) => originalPosition + speed / secondsPerDay;
        const angles = [
            types_1.Aspect.getAngleBetweenLongitudes(calculatePosition(longitude1, -longitudeSpeed1), calculatePosition(longitude2, -longitudeSpeed2)),
            types_1.Aspect.getAngleBetweenLongitudes(longitude1, longitude2),
            types_1.Aspect.getAngleBetweenLongitudes(calculatePosition(longitude1, longitudeSpeed1), calculatePosition(longitude2, longitudeSpeed2))
        ];
        return angles[0] > angles[1] && angles[1] < angles[2]; // before > now, and now < after (now is in a valley in between before and after, and cannot get any lower).
    }
    /** Returns `true` if `angle1` and `angle2` straddle referenceAngle. */
    static isCrossingOverAngle(angle1, angle2, referenceAngle) {
        // all other cases
        return (angle1 == referenceAngle && angle2 != referenceAngle) || (angle2 == referenceAngle && angle1 != referenceAngle) ||
            (angle1 < referenceAngle && angle2 > referenceAngle) || (angle1 > referenceAngle && angle2 < referenceAngle);
    }
}
exports.PlanetaryAspect = PlanetaryAspect;
/** Encapsulates a planetary phenomena, which is really the phase information of a planet, usually the moon. */
class Phenomena extends PlanetaryEphemerisData {
    constructor(date, data) {
        super(date);
        this.planet = data.planet;
        this.phaseAngle = data.phaseAngle;
        this.waxing = data.waxing;
        this.illumination = data.phase;
        this.phase = types_1.Phase.getPhase(types_1.Phase.getFullAngle(this.phaseAngle, this.waxing));
    }
}
exports.Phenomena = Phenomena;
/** Represents a house with given number, cusp, and end degree. */
class HousePosition extends PlanetaryEphemerisData {
    /** The index of the house 0-11. */
    get index() { return this.number - 1; }
    constructor(date, number, cusp, endDegree) {
        super(date);
        this.number = number;
        this.cusp = cusp;
        this.end = endDegree;
    }
}
exports.HousePosition = HousePosition;
/** Retrograde status for a given planet and date. */
class RetrogradePosition extends PlanetaryEphemerisData {
    /** True if direct, false if retrograde. */
    get direct() { return !this.retrograde; }
    constructor(date, planet, retrograde) {
        super(date);
        this.planet = planet;
        this.retrograde = retrograde;
    }
}
exports.RetrogradePosition = RetrogradePosition;
/** A change in sign, retrograde, phase, or aspect status at a given date. */
class ChangeEvent {
    constructor(date, type) {
        type = ChangeEvent.types[type];
        if (!type) {
            console.error('Invalid ChangeEvent type: ' + type);
            return;
        }
        this.date = date;
        this.type = type;
    }
    get data() {
        return { date: this.date.valueOf(), type: this.type };
    }
}
/** Event change types. */
ChangeEvent.types = { sign: 'sign', retrograde: 'retrograde', aspect: 'aspect', phase: 'phase', moon: 'moon', natal: 'natal' };
exports.ChangeEvent = ChangeEvent;
class SignChangeEvent extends ChangeEvent {
    constructor(date, planet, previousPlanet) {
        super(date, ChangeEvent.types.sign);
        this.planet = planet;
        this.previousPlanet = previousPlanet;
    }
}
exports.SignChangeEvent = SignChangeEvent;
class AspectChangeEvent extends ChangeEvent {
    constructor(date, aspect) {
        super(date, ChangeEvent.types.aspect);
        this.aspect = aspect;
    }
}
exports.AspectChangeEvent = AspectChangeEvent;
class NatalTransitEvent extends ChangeEvent {
    constructor(date, type, birthDate, latitude, longitude, data) {
        super(date, ChangeEvent.types.natal);
        this.type = type;
        this.birthDate = birthDate;
        this.latitude = latitude;
        this.longitude = longitude;
        this.natalHouse = data.natalHouse;
        this.natalPlanet = data.natalPlanet;
        this.transitingPlanet = data.transitingPlanet;
    }
}
exports.NatalTransitEvent = NatalTransitEvent;
class RetrogradeChangeEvent extends ChangeEvent {
    get retrograde() { return this.planet.retrograde; }
    constructor(date, planet) {
        super(date, ChangeEvent.types.retrograde);
        this.planet = planet;
    }
}
exports.RetrogradeChangeEvent = RetrogradeChangeEvent;
class PhaseChangeEvent extends ChangeEvent {
    constructor(date, phase) {
        super(date, ChangeEvent.types.phase);
        this.phase = phase;
    }
}
exports.PhaseChangeEvent = PhaseChangeEvent;
class MoonChangeEvent extends ChangeEvent {
    get types() {
        return ['apogee', 'perigee', 'an', 'dn'].filter(f => this[f]);
    }
    constructor(date, moon, perigee, apogee, an, dn) {
        super(date, ChangeEvent.types.moon);
        this.moon = moon;
        this.perigee = perigee;
        this.apogee = apogee;
        this.an = an;
        this.dn = dn;
    }
}
exports.MoonChangeEvent = MoonChangeEvent;
