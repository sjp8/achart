"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const ava_1 = require("ava");
const ephemeris_1 = require("./ephemeris");
const swisseph = require("swisseph");
const moment = require("moment");
let lastEphemeris;
const getEphemeris = () => lastEphemeris || new ephemeris_1.Ephemeris(ephemeris_1.Ephemeris.dataSources.swisseph, swisseph);
ava_1.default('ephemeris loads and can calculate planetary positions', t => {
    const ephemeris = getEphemeris();
    let now = moment();
    let before = moment().add(-1, 'minutes');
    let sun = ephemeris.getPlanet(now, 'Sun');
    let moon = ephemeris.getPlanet(now, 'Moon');
    let sun0 = ephemeris.getPlanet(before, 'Sun');
    let moon0 = ephemeris.getPlanet(before, 'Moon');
    t.truthy(sun);
    t.truthy(ephemeris.getAspect(sun, moon, sun0, moon0));
    t.truthy(ephemeris.getHouses(now, 0, 30));
    let moonR = ephemeris.getRetrograde(now, 'Sun');
    t.not(moonR, null);
    t.skip.false(moonR); // moon should never be retrograde
});
ava_1.default('speed of aspect calculation', t => {
    const ephemeris = getEphemeris();
    const now = moment();
    // ephemeris.
});
