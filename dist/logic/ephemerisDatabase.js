"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose = require("mongoose");
const moment = require("moment");
const env_1 = require("../env");
const databaseURI = env_1.default.MONGODB_URI;
mongoose.Promise = global.Promise;
const event_schema_1 = require("../schema/event.schema");
const coordinatePlaces = 6;
exports.roundCoordinate = (coord, places = coordinatePlaces) => Number(coord.toFixed(places));
/** Constants for each table. */
const schemaTypes = ['aspect', 'retrograde', 'moon', 'phase', 'sign'];
/** Retrieve the schema associated with the given type, for querying MongoDB. */
const getSchemaForType = (type) => {
    const schemaMap = {
        aspect: event_schema_1.AspectChange,
        retrograde: event_schema_1.RetrogradeChange,
        moon: event_schema_1.MoonChange,
        phase: event_schema_1.PhaseChange,
        sign: event_schema_1.SignChange,
        natal: event_schema_1.NatalChange
    };
    return schemaMap[type];
};
/** An interface for adding, updating and retrieving records with the database. */
class EphemerisDatabase {
    connect() {
        return mongoose.connect(databaseURI, { reconnectTries: 2 })
            .then(() => console.log('connected to mongodb'))
            .catch(err => {
            console.error('error connecting to mongodb.', err);
        });
    }
    /** Gets the maximum date for all tables. */
    getLatestDate() {
        const getAggregateQuery = (model) => model.aggregate([
            { $sort: { date: -1 } },
            { $group: { _id: 'all', maxDate: { "$first": "$date" } } }
        ])
            .exec()
            .then(result => moment.utc(result.maxDate));
        const tables = ['aspect', 'retrograde', 'phase', 'sign', 'moon'];
        const queries = [event_schema_1.AspectChange, event_schema_1.RetrogradeChange, event_schema_1.PhaseChange, event_schema_1.SignChange, event_schema_1.MoonChange]
            .map(schema => getAggregateQuery(schema));
        return Promise.all(queries)
            .then(dates => {
            const result = {};
            tables.forEach((table, i) => result[table] = dates[i]);
            return result;
        });
    }
    /** Gets a change by mongo ID. */
    getChangeById(eventId, type) {
        const schema = getSchemaForType(type);
        return schema.findById(eventId).lean().exec().then(result => result);
    }
    getSequentialChanges(fromEventId, type, beforeCount, afterCount) {
        const schema = getSchemaForType(type);
        if (!schema) {
            console.error('no schema found for type ', type);
            return Promise.reject('no schema found for type ' + type);
        }
        const selectPlanetField = type === 'aspect' ? 'planets' : (type === 'natal' ? 'natalPlanet transitingPlanet birthDate latitude longitude' : 'planet');
        return schema.findOne({ _id: fromEventId }).select('_id date ' + selectPlanetField).lean().exec()
            .then((result) => {
            return {
                date: result.date,
                _id: result._id,
                planets: result.planets,
                planet: result.planet,
                natalPlanet: result.natalPlanet && result.natalPlanet.name,
                transitingPlanet: result.transitingPlanet && result.transitingPlanet.name,
                latitude: result.latitude,
                longitude: result.longitude,
                birthDate: result.birthDate
            };
        })
            .then(({ date, _id, planet, planets, natalPlanet, transitingPlanet, latitude, longitude, birthDate }) => {
            const criteria = { date: { $lte: date } };
            if (planet) {
                criteria['planet'] = planet;
            }
            else if (planets) {
                const or = [
                    { 'planets.0.name': planets[0].name, 'planets.1.name': planets[1].name },
                    { 'planets.1.name': planets[0].name, 'planets.0.name': planets[1].name }
                ];
                criteria['$or'] = or;
                if (type === 'aspect') {
                    criteria['exact'] = true;
                }
            }
            else if (transitingPlanet || natalPlanet) {
                criteria['birthDate'] = birthDate;
                criteria['latitude'] = latitude;
                criteria['longitude'] = longitude;
                transitingPlanet && (criteria['transitingPlanet.name'] = transitingPlanet);
                natalPlanet && (criteria['natalPlanet.name'] = natalPlanet);
            }
            return schema
                .find(criteria)
                .sort({ date: -1 })
                .limit(beforeCount + 1)
                .select('_id date')
                .lean().exec().then((backward) => {
                const date = backward.length > 0 ? backward[backward.length - 1].date : null;
                return Object.assign({}, criteria, { date: { $gte: date } });
            });
        })
            .then(criteria => {
            return criteria ?
                schema
                    .find(criteria)
                    .sort({ date: 1 })
                    .limit(beforeCount + afterCount + 1) // an even number of events on either side of the reference event
                    .lean().exec()
                    .then(result => result)
                : null;
        });
    }
    getNatalChanges(birthTimestamp, latitude, longitude, fromDate, toDate) {
        const date = { $gte: fromDate.toDate(), $lt: toDate.toDate() };
        return event_schema_1.NatalChange
            .find({ date, latitude: exports.roundCoordinate(latitude), longitude: exports.roundCoordinate(longitude), birthDate: birthTimestamp })
            .sort({ date: 1 })
            .lean()
            .exec()
            .then(result => result);
    }
    /**
     * Gets a list of changes for each table, for the given range of time.
     * Populates EventDocument.averageFrequency for all events, and AspectEventDocument.retrogrades and AspectEventDocument.series.
     */
    getChanges(fromDate, toDate, type) {
        let date = { $gte: fromDate.toDate(), $lt: toDate.toDate() };
        const types = type ? [type] : schemaTypes;
        const queries = types.map(type => getSchemaForType(type).find({ date }).sort({ date: 1 }).lean());
        return Promise.all(queries)
            .then((results) => {
            const result = [];
            results.forEach(r => result.push(...r));
            result.sort((a, b) => a.date.valueOf() > b.date.valueOf() ? 1 : -1);
            return result;
        });
    }
    saveMoonEvent(event) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield Promise.all(event.types.map((type) => __awaiter(this, void 0, void 0, function* () {
                const doc = new event_schema_1.MoonChange({
                    type: 'moon',
                    date: event.date.toDate(),
                    event: type,
                    longitude: event.moon.longitude,
                    latitude: event.moon.latitude,
                    distance: event.moon.distance
                });
                return yield doc.save();
            })));
        });
    }
    savePhaseEvent(event) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield new event_schema_1.PhaseChange({
                type: 'phase',
                date: event.date.toDate(),
                planet: event.phase.planet,
                longitude: event.phase.longitude,
                distance: event.phase.distance,
                sunLongitude: event.phase.sunLongitude,
                phase: event.phase.phase.name,
                angle: event.phase.angle,
                waxing: event.phase.waxing
            });
        });
    }
    saveSignEvent(event) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield new event_schema_1.SignChange({
                type: 'sign',
                date: event.date.toDate(),
                planet: event.planet.planet,
                sign: event.planet.sign.name,
                longitude: event.planet.longitude,
                retrograde: event.planet.retrograde
            }).save();
        });
    }
    saveRetrogradeEvent(event) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield new event_schema_1.RetrogradeChange({
                type: 'retrograde',
                date: event.date.toDate(),
                planet: event.planet.planet,
                retrograde: event.retrograde,
                longitude: event.planet.longitude
            }).save();
        });
    }
    saveAspectEvent(event) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield new event_schema_1.AspectChange({
                type: 'aspect',
                date: event.date.toDate(),
                planets: [event.aspect.planet1, event.aspect.planet2]
                    .map(p => ({ name: p.planet, longitude: p.longitude, retrograde: p.retrograde })),
                aspect: event.aspect.aspect.name,
                major: event.aspect.aspect.major,
                applying: event.aspect.applying,
                planetsSeparating: event.aspect.planetsSeparating,
                exact: event.aspect.exact
            }).save();
        });
    }
    saveNatalEvent(event) {
        return __awaiter(this, void 0, void 0, function* () {
            const latitude = exports.roundCoordinate(event.latitude);
            const longitude = exports.roundCoordinate(event.longitude);
            const existingNatal = yield event_schema_1.NatalChange.findOne({
                event: event.type,
                birthDate: event.birthDate,
                latitude,
                longitude,
                date: event.date.toDate()
            }).exec();
            if (existingNatal) {
                console.log('existing natal event: ', JSON.stringify(existingNatal.toJSON()));
            }
            return existingNatal || (yield new event_schema_1.NatalChange({
                type: 'natal',
                birthDate: event.birthDate,
                latitude,
                longitude,
                date: event.date.toDate(),
                event: event.type,
                natalPlanet: event.natalPlanet ? { name: event.natalPlanet.planet, longitude: event.natalPlanet.longitude, retrograde: event.natalPlanet.retrograde } : null,
                transitingPlanet: event.transitingPlanet ? { name: event.transitingPlanet.planet, longitude: event.transitingPlanet.longitude, retrograde: event.transitingPlanet.retrograde } : null,
                natalHouse: event.natalHouse
            }).save());
        });
    }
}
exports.EphemerisDatabase = EphemerisDatabase;
