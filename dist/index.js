"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
// Setup basic express app
const express_1 = require("./server/express");
// Setup routes
const routes_1 = require("./server/routes");
routes_1.addRoutes(express_1.app);
// Determine port to listen on, defaults to 7444.
const port = process.env.PORT || 7444;
// Start server
express_1.app.listen(port, err => {
    if (err)
        console.error(err);
    else
        console.log('listening on ' + port);
});
