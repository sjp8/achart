"use strict";
// gcloud beta pubsub topics publish populate_natal_background --message '{"startDate":"2018-05-01T00:00:00.000Z","endDate":"2018-06-01T00:00:00.000Z","birthDate":"1988-02-03T19:46:00.000Z","longitude":-118.2157933,"latitude":34.2048286}'
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const moment = require("moment");
const ephemerisDatabase_1 = require("./logic/ephemerisDatabase");
const ephemeris_1 = require("./logic/astrology/ephemeris");
function populate_natal_background(event) {
    return __awaiter(this, void 0, void 0, function* () {
        const ephemerisDatabase = new ephemerisDatabase_1.EphemerisDatabase();
        try {
            yield ephemerisDatabase.connect();
        }
        catch (err) {
            return Promise.reject(err);
        }
        return new Promise((resolve, reject) => {
            const message = event.data;
            const data = message.data ? JSON.parse(Buffer.from(message.data, 'base64').toString()) : null;
            if (data) {
                const birthDate = moment(data.birthDate);
                const startDate = moment(data.startDate);
                const endDate = moment(data.endDate);
                const latitude = Number(data.latitude);
                const longitude = Number(data.longitude);
                if ([birthDate, startDate, endDate].some(date => !date.isValid())) {
                    console.error('invalid birth, start, or end date.');
                    return reject('date format');
                }
                if ([latitude, longitude].some(coord => isNaN(coord))) {
                    console.error('invalid latitude or longitude. Must provide valid numerical values.');
                    return reject('coordinate format');
                }
                console.log('searching for natal changes with the following args: ' + JSON.stringify(data) + '.');
                const natalPositions = ephemeris_1.ephemeris.getPlanetsArray(birthDate);
                const natalHouses = ephemeris_1.ephemeris.getHouses(birthDate, longitude, latitude);
                ephemeris_1.ephemeris.searchNatalChanges(birthDate.unix(), latitude, longitude, natalPositions, natalHouses, startDate, endDate, (change) => __awaiter(this, void 0, void 0, function* () {
                    console.log('change found, saving. ' + change.transitingPlanet.planet + ' type ' + change.type);
                    try {
                        const doc = yield ephemerisDatabase.saveNatalEvent(change);
                        console.log('change saved ', doc._id);
                    }
                    catch (ex) {
                        console.error('error saving natal event for ' + change.birthDate + ', ' + change.latitude + ', ' + change.longitude + '.');
                        console.error(ex);
                    }
                }), () => {
                    console.log('search natal changes done');
                    setTimeout(() => __awaiter(this, void 0, void 0, function* () {
                        console.log('exiting function');
                        resolve();
                    }), 10000);
                });
            }
        });
    });
}
exports.populate_natal_background = populate_natal_background;
