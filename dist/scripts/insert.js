"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const ephemerisDatabase_1 = require("../logic/ephemerisDatabase");
const event_schema_1 = require("../schema/event.schema");
const astrology_1 = require("../logic/astrology");
const moment = require("moment");
const program = require("commander");
const mongoose = require("mongoose");
const swisseph = require("swisseph");
program
    .version('2.2.0')
    .option('-f, --from <date>', 'The date from which to insert changes.', moment.utc)
    .option('-t, --to <date>', 'The maximum date for which to insert changes.', moment.utc)
    .option('-y, --years <n>', 'If delete is not set, can optionally specify a number of additional years for each table.', Number, 0)
    .option('-d, --delete', 'Delete all data from the database (or just the specified table) before starting.')
    .option('--aspects', 'Insert aspect events.')
    .option('--phases', 'Insert phase events.')
    .option('--signs', 'Insert sign events.')
    .option('--retrogrades', 'Insert retrograde events.')
    .option('--moons', 'Insert Moon-related events other than phase.')
    .option('--natal', 'Insert natal events (requires latitude, longitude, and date parameters).')
    .option('--latitude <n>', 'Specify latitude for natal events.', n => ephemerisDatabase_1.roundCoordinate(Number(n)))
    .option('--longitude <n>', 'Specify longitude for natal events.', n => ephemerisDatabase_1.roundCoordinate(Number(n)))
    .option('--date <date>', 'Specify the birth date in an ISO-compatible string, or timestamp, for natal events.')
    .parse(process.argv);
console.log('years: ' + program.years);
const startTime = moment();
const ephemeris = new astrology_1.Ephemeris('swisseph', swisseph);
const insertAll = !program.aspects && !program.phases && !program.signs && !program.retrogrades && !program.moons && !program.natal;
if (insertAll) {
    program.aspects = program.phases = program.signs = program.retrogrades = program.moons = program.natal = true;
}
const deleteTable = (table, callback) => {
    console.log('deleting from table: ' + table.modelName);
    table.remove({}, callback);
};
const insert = () => __awaiter(this, void 0, void 0, function* () {
    console.log('start querying changes');
    const maxDateByTable = yield database.getLatestDate();
    const getDateRange = (table) => {
        const max = program.delete ? null : maxDateByTable[table];
        const years = program.years;
        // if there is a maximum date, use the next second as the starting point. otherwise start today
        const rangeMin = program.from ? program.from : (max ? moment.utc(max).add(1, 'second') : moment.utc().startOf('day'));
        // if the number of years is set, add to rangeMin. otherwise, use `to` parameter.
        const rangeMax = program.to ? program.to : (years ? moment.utc(rangeMin).startOf('day').add(years, 'years') : moment.utc(program.to).endOf('day'));
        return {
            min: rangeMin,
            max: rangeMax
        };
    };
    const generateFunction = (model, tableName, searchFunction) => {
        return new Promise((resolve, reject) => {
            if (!program[tableName]) {
                console.log(`skipping ${tableName}`);
                resolve();
            }
            else {
                console.log(`starting ${tableName}...`);
                const performSearch = () => {
                    // track save progress in order to call done() at the appropriate time
                    let eventCount = 0;
                    // get the appropriate date range given the command line parameters and the maximum event date in the table
                    const range = getDateRange(tableName);
                    // we ask the search function to let us save documents by calling the given callback.
                    searchFunction(range, (doc, log) => {
                        // every time a document needs to be saved, increment
                        eventCount += 1;
                        doc.then(doc => {
                            // decrement, then check for completeness
                            eventCount -= 1;
                            if (eventCount === 0) {
                                resolve();
                            }
                            log(doc);
                        }, err => {
                            console.error(err);
                        });
                    });
                };
                const performDelete = (done) => {
                    if (program.delete) {
                        deleteTable(model, done);
                    }
                    else {
                        done();
                    }
                };
                performDelete(() => {
                    performSearch();
                });
            }
        });
    };
    const result = {};
    result.moons = yield generateFunction(event_schema_1.MoonChange, 'moons', (range, saveDoc) => {
        ephemeris.searchMoonEventChanges(range.min, range.max, (change) => {
            saveDoc(database.saveMoonEvent(change), doc => console.log(`moon doc saved: ${change.date.toString()}, ${doc.type}, longitude: ${doc.longitude}, latitude: ${doc.latitude}, distance: ${doc.distance}`));
        }, () => console.log("Finished searching moon changes."));
    });
    result.natal = generateFunction(event_schema_1.NatalChange, 'natal', (range, saveDoc) => {
        const birthDate = moment(program.date);
        const natalPositions = ephemeris.getPlanetsArray(birthDate);
        const latitude = program.latitude;
        const longitude = program.longitude;
        const natalHouses = ephemeris.getHouses(birthDate, longitude, latitude);
        ephemeris.searchNatalChanges(birthDate.unix(), latitude, longitude, natalPositions, natalHouses, range.min, range.max, (change) => {
            saveDoc(database.saveNatalEvent(change), doc => console.log(`natal doc saved: ${change.date.toString()}, ${doc.type}, ${doc.event}, house: ${doc.natalHouse}, transiting: ${doc.transitingPlanet ? doc.transitingPlanet.name : 'none'}, natal: ${doc.natalPlanet ? doc.natalPlanet.name : 'none'}`));
        }, () => console.log('Finished searching natal events.'));
    });
    result.aspects = generateFunction(event_schema_1.AspectChange, 'aspects', (range, saveDoc) => {
        ephemeris.searchAspectChanges(range.min, range.max, (change) => {
            if (!change.aspect.hasAspect) {
                console.log('invalid: change lacks an aspect.');
                return;
            }
            saveDoc(database.saveAspectEvent(change), doc => console.log('aspect doc saved: ' + change.date.toString() + ' ' + doc.aspect + ' ' + doc.planets[0].name + ' and ' + doc.planets[1].name + (doc.exact ? ' exact' : '')));
        }, () => console.log('Finished searching aspects.'));
    });
    result.phases = generateFunction(event_schema_1.PhaseChange, 'phases', (range, saveFunction) => {
        ephemeris.searchMoonPhaseChanges(range.min, range.max, (event) => {
            saveFunction(database.savePhaseEvent(event), doc => console.log('phase doc saved: ' + doc.date.toString() + ' ' + doc.planet + ' ' + doc.phase));
        }, () => console.log('Finished searching phases.'));
    });
    result.signs = generateFunction(event_schema_1.SignChange, 'signs', (range, saveFunction) => {
        ephemeris.searchSignChanges(range.min, range.max, (change) => {
            saveFunction(database.saveSignEvent(change), doc => console.log('sign doc saved: ' + doc.date.toString() + ' ' + doc.planet + ' ' + doc.sign));
        }, () => console.log('Finished searching signs.'));
    });
    result.retrogrades = generateFunction(event_schema_1.RetrogradeChange, 'retrogrades', (range, saveFunction) => {
        ephemeris.searchRetrogradeChanges(range.min, range.max, (change) => {
            saveFunction(database.saveRetrogradeEvent(change), doc => console.log('retrograde doc saved: ' + change.date.toString() + ' ' + doc.planet + ' ' + (change.retrograde ? 'retrograde' : 'direct')));
        }, () => console.log('Finished searching retrogrades.'));
    });
    console.log('Finished adding changes.');
    const time = moment.duration(moment().diff(startTime)).asMinutes();
    console.log(`Took ${time} minutes.`);
    if (mongoose.connection) {
        mongoose.disconnect();
    }
});
const database = new ephemerisDatabase_1.EphemerisDatabase();
database.connect().then(insert);
