const log = (str, json = false) => print(json ? JSON.stringify(str, null, "\t") : str);
class SequentialEventMap {
    constructor() {
        this.events = {};
    }
    /** Adds an event to the list marked by `key` */
    addEvent(key, event) {
        this.events[key] = this.events[key] || [];
        this.events[key].push(event);
    }
    /** Returns all events associated with the given key (see keyFunctions). */
    getEvents(key) {
        return this.events[key] || [];
    }
    /** Gets a list of all event list keys. */
    getKeys() {
        return Object.keys(this.events);
    }
}
/**
 * Functions to generate the `key` for a series of events, for occurrences (the same exact event),
 * and sequence events (what happens directly following or preceding this event).
 */
SequentialEventMap.keyFunctions = {
    /** key functions for occurrences. */
    occurrence: {
        aspect(event) { return 'aspect-' + event.planets[0].name + '-' + event.planets[1].name + '-' + event.aspect; },
        sign(event) { return 'sign-' + event.planet + '-' + event.sign; },
        phase(event) { return 'phase-' + event.planet + '-' + event.phase; },
        retrograde(event) { return 'retrograde-' + event.planet + '-' + (event.retrograde ? 'retrograde' : 'direct'); },
        moon(event) { return 'moon-' + event.type; }
    },
    /** key functions for sequences. */
    sequence: {
        aspect(event) { return 'aspect-' + event.planets[0].name + '-' + event.planets[1].name; },
        sign(event) { return 'sign-' + event.planet; },
        phase(event) { return 'phase-' + event.planet; },
        retrograde(event) { return 'retrograde-' + event.planet; },
        moon(event) { return 'moon-event'; }
    }
};
/**
 * Class that processes and populates astrology event metadata.
 */
class AstrologyEventMetadataProcessor {
    /** Process fields specific to aspects: start/endDate and exactDates. These fields track where the aspect started (entered orb),
     * and continues to track exactDates (if there is a retrograde and multiple moments of exactness), up until the aspect ends (left orb),
     * at which point all events from start to end are given this same metadata. */
    processAspectDates() {
        // add startDate, exactDates, and endDate to all aspects
        log('processing aspect dates');
        const aspectMap = {};
        /** Generate a key for this aspect so that like events can be tracked in the same list. */
        const aspectKey = (change) => change.planets[0].name + '-' + change.planets[1].name + '-' + change.aspect;
        // process the list of events in aspectchanges, organized by key: planet1-planet2-aspect
        const changes = db.aspectchanges.find().sort({ date: 1 }).forEach(change => {
            const key = aspectKey(change);
            // If the event is entering, start a new list for this sequence.
            if (change.applying && !change.exact) {
                aspectMap[key] = [change];
            }
            else if (!aspectMap[key]) {
                // skip, maybe this is an event at the beginning of the list that started mid-aspect, e.g. exact or leaving.
                // wait for the next "entered" event to get started.
            }
            else if (change.exact) {
                aspectMap[key].push(change);
            }
            else { // leaving
                aspectMap[key].push(change);
                // build the dates field which will be added to all aspects
                const dates = { startDate: null, endDate: null, exactDates: [] };
                for (let i = 0; i < aspectMap[key].length; i++) {
                    const d = aspectMap[key][i].date;
                    if (i == 0)
                        dates.startDate = d;
                    else if (i == aspectMap[key].length - 1)
                        dates.endDate = d;
                    else
                        dates.exactDates.push(d);
                }
                for (let i = 0; i < aspectMap[key].length; i++) {
                    db.aspectchanges.update({ _id: aspectMap[key][i]._id }, { $set: dates });
                }
                // once this series of events has been processed, restart.
                aspectMap[key] = undefined;
            }
        });
        log('executing bulk aspect change updates');
    }
    /** Helper function used to add an event to the master list. */
    scribeEvent(map, event, eventKey, collection, isValidNext, isValidPrevious, setNextFields = {}, setPreviousFields = {}) {
        map.addEvent(eventKey, {
            _id: event._id,
            date: event.date,
            collection,
            isValidNext,
            isValidPrevious,
            setNextFields,
            setPreviousFields
        });
    }
    // process any map (occurrence or next event)
    processNextPreviousDates(map, nextFields, previousFields) {
        const types = ['aspect', 'sign', 'phase', 'retrograde', 'moon'];
        const content = {};
        types.forEach(function (type) {
            content[type] = [];
        });
        map.getKeys().forEach((key) => {
            const events = map.getEvents(key);
            log('processing ' + events.length + ' events in series: ' + key);
            const type = key.substring(0, key.indexOf('-'));
            for (let i = 0; i < events.length - 1; i++) {
                const event = events[i];
                let nextEvent;
                let previousEvent;
                if (event.isValidNext) {
                    for (let j = i + 1; j < events.length; j++) {
                        if (events[j].isValidNext) {
                            nextEvent = events[j];
                            break;
                        }
                    }
                }
                if (event.isValidPrevious) {
                    for (var j = i - 1; j >= 0; j--) {
                        if (events[j].isValidPrevious) {
                            previousEvent = events[j];
                            break;
                        }
                    }
                }
                // if the next event is available, set next fields.
                if (nextEvent) {
                    let setFields = nextEvent.setNextFields || {};
                    // set generic fields based on the category configuration
                    for (const field in nextFields) {
                        setFields[field] = nextFields[field](nextEvent);
                    }
                    content[type].push({
                        updateOne: {
                            filter: { _id: event._id },
                            update: { $set: setFields }
                        }
                    });
                }
                // if the previous event is available, set previous fields
                if (previousEvent) {
                    let setFields = previousEvent.setPreviousFields || {};
                    for (let field in previousFields) {
                        setFields[field] = previousFields[field](previousEvent);
                    }
                    content[type].push({
                        updateOne: {
                            filter: { _id: event._id },
                            update: { $set: setFields }
                        }
                    });
                }
            }
        });
        for (const byType in content) {
            const collectionName = byType + 'changes';
            if (content[byType].length > 0) {
                log('performing bulk update of type ' + byType + ' with ' + content[byType].length + ' operations');
                const bulk = db[collectionName].initializeUnorderedBulkOp();
                content[byType].forEach((item) => {
                    bulk.find(item.updateOne.filter).updateOne(item.updateOne.update);
                });
                bulk.execute();
            }
        }
    }
    /** Populate next/previous event/occurrence data. */
    processNextAndPrevious() {
        log('building next/previous event/occurrence data');
        const occurrenceMap = new SequentialEventMap();
        const sequentialMap = new SequentialEventMap();
        log('phasechanges');
        db.phasechanges.find().sort({ date: 1 }).forEach((event) => {
            this.scribeEvent(occurrenceMap, event, SequentialEventMap.keyFunctions.occurrence.phase(event), 'phasechanges', true, true);
            this.scribeEvent(sequentialMap, event, SequentialEventMap.keyFunctions.sequence.phase(event), 'phasechanges', true, true);
        });
        log('aspectchanges');
        db.aspectchanges.find().sort({ date: 1 }).forEach((event) => {
            this.scribeEvent(occurrenceMap, event, SequentialEventMap.keyFunctions.occurrence.aspect(event), 'aspectchanges', event.exact, true);
            this.scribeEvent(sequentialMap, event, SequentialEventMap.keyFunctions.sequence.aspect(event), 'aspectchanges', event.exact, true, { nextEventAspect: event.aspect }, { previousEventAspect: event.aspect });
        });
        log('signchanges');
        db.signchanges.find().sort({ date: 1 }).forEach((event) => {
            this.scribeEvent(occurrenceMap, event, SequentialEventMap.keyFunctions.occurrence.sign(event), 'signchanges', true, true);
            this.scribeEvent(sequentialMap, event, SequentialEventMap.keyFunctions.sequence.sign(event), 'signchanges', true, true, { nextEventSign: event.sign }, { previousEventSign: event.sign });
        });
        log('retrogradechanges');
        db.retrogradechanges.find().sort({ date: 1 }).forEach((event) => {
            this.scribeEvent(occurrenceMap, event, SequentialEventMap.keyFunctions.occurrence.retrograde(event), 'retrogradechanges', true, true, { nextOccurrenceLongitude: event.longitude }, { previousOccurrenceLongitude: event.longitude });
            this.scribeEvent(sequentialMap, event, SequentialEventMap.keyFunctions.sequence.retrograde(event), 'retrogradechanges', true, true, { nextEventLongitude: event.longitude }, { previousEventLongitude: event.longitude });
        });
        log('moonchanges');
        db.moonchanges.find().sort({ date: 1 }).forEach(event => {
            this.scribeEvent(occurrenceMap, event, SequentialEventMap.keyFunctions.occurrence.moon(event), 'moonchanges', true, true);
            this.scribeEvent(sequentialMap, event, SequentialEventMap.keyFunctions.sequence.moon(event), 'moonchanges', true, true);
        });
        // process/save next/prev occurrences
        log('processing and saving next/previous occurrences');
        this.processNextPreviousDates(occurrenceMap, {
            nextOccurrence: function (event) { return event._id; },
            nextOccurrenceDate: function (event) { return event.date; }
        }, {
            previousOccurrence: function (event) { return event._id; },
            previousOccurrenceDate: function (event) { return event.date; }
        });
        // process/save next/prev events
        log('processing and saving next/previous events');
        this.processNextPreviousDates(sequentialMap, {
            nextEvent: function (event) { return event._id; },
            nextEventDate: function (event) { return event.date; }
        }, {
            previousEvent: function (event) { return event._id; },
            previousEventDate: function (event) { return event.date; }
        });
        log('changes processed');
    }
    /** Process retrograde fixes */
    processRetrogradeFixes() {
        // there is a quirk in swiss ephemeris where around the exact moment of changing retrograde or direct,
        // it switches back and forth between direct and retrograde for a few seconds. This function fixes the problem.
        // There is also a potential problem of two of the same direction changes in a row, which is invalid.
        // the retrograde event with the most extreme longitude is the correct one
        // when turning retrograde (denoted by first event in sequence of a day), looking for highest longitude
        // when turning direct, looking for lowest longitude (most extreme moment of retrograde period)
        const retrogrades = db.retrogradechanges.find({}, { date: true, planet: true, retrograde: true, longitude: true, _id: true }).sort({ date: 1 }).toArray();
        log('processing ' + retrogrades.length + ' retrogrades');
        const last = {};
        const sameLog = {};
        const bulk = db.retrogradechanges.initializeUnorderedBulkOp();
        retrogrades.forEach((r) => {
            const lastEvent = last[r.planet];
            if (!lastEvent) {
                last[r.planet] = r;
                return;
            }
            // if there are several switches within the same longitude, use the most extreme one as it is correct.
            const sameLongitude = Math.abs(lastEvent.longitude - r.longitude) < 0.001;
            if (sameLongitude) {
                // add to the same log (along with the last event, which is the first in the series))
                if (!sameLog[r.planet]) {
                    sameLog[r.planet] = [lastEvent];
                }
                sameLog[r.planet].push(r);
                last[r.planet] = r;
            }
            else if (sameLog[r.planet] && sameLog[r.planet].length) {
                // process the sameLog if there is one by removing all except the most extreme case
                const planetSameLog = sameLog[r.planet];
                const retrograde = planetSameLog[planetSameLog.length - 1].retrograde; // the first and last logs both hold the same correct retrograde value
                let maxEvent = null;
                const isNewMax = (longitude) => {
                    const func = retrograde ? Math.max : Math.min;
                    return maxEvent == null || func(longitude, maxEvent.longitude) == longitude;
                };
                planetSameLog.forEach((e) => {
                    const correctRetrograde = e.retrograde == retrograde;
                    if (correctRetrograde) {
                        if (isNewMax(e.longitude)) {
                            maxEvent = e;
                        }
                    }
                });
                const deleteIds = [];
                planetSameLog.forEach(function (e) {
                    if (e._id != maxEvent._id) {
                        deleteIds.push(e._id);
                    }
                });
                if (deleteIds.length) {
                    log('deleting ' + deleteIds.length + ' duplicate retrograde events');
                    log('fixed retrograde switch problem for ' + r.planet + ' on ' + maxEvent.date + ' (' + maxEvent._id + ')');
                    bulk.find({ _id: { $in: deleteIds } }).remove();
                }
                sameLog[r.planet] = undefined;
                last[r.planet] = maxEvent;
            }
            else {
                last[r.planet] = r;
            }
        });
        log('bulk removing retrograde fixes, if any.');
        bulk.execute();
    }
    /** Add signEvents to retrogradechanges collection. */
    processRetrogradeSigns() {
        log('processing retrograde sign events');
        const bulk = db.retrogradechanges.initializeUnorderedBulkOp();
        // find all retrogrades
        const retrogrades = db.retrogradechanges.find({}, { date: true, planet: true, retrograde: true, longitude: true, _id: true }).sort({ date: 1 }).toArray();
        for (let i = 0; i < retrogrades.length - 1; i++) {
            const current = retrogrades[i];
            const next = retrogrades[i + 1];
            const signs = [];
            // add sign changes of the same planet, between the current retrograde's date and the start of the next retrograde event.
            db.signchanges.find({ planet: current.planet, date: { $gte: current.date, $lt: next.date } }).forEach((sign) => {
                signs.push({
                    date: sign.date,
                    sign: sign.sign,
                    event: sign._id
                });
            });
            bulk.find({ _id: current._id }).updateOne({ $set: { signEvents: signs } });
        }
        bulk.execute();
    }
    /** Add moon phase data to moon sign events. */
    processSignEventMoonPhases() {
        log('add moon phase data to moon\'s sign events.');
        const phases = db.phasechanges.find().sort({ date: 1 }).toArray();
        const signs = db.signchanges.find({ planet: 'Moon' }).sort({ date: 1 }).toArray();
        // if first sign object is before the first phase object, skip signs until at 
        // least one phase object is before the sign object
        // ensuring that the correct moon phase event is available for the sign.
        while (signs[0].date.valueOf() < phases[0].date.valueOf()) {
            signs.shift();
        }
        // assign phases to each sign event
        const bulk = db.signchanges.initializeUnorderedBulkOp();
        signs.forEach((sign) => {
            if (phases.length <= 1) {
                return;
            }
            const date = sign.date.valueOf();
            const phaseIsBeforeSign = () => { return phases[0].date.valueOf() <= date; };
            const nextPhaseIsAfterSign = () => { return phases[1].date.valueOf() > date; };
            // skip moon phase events while there are phases to skip,
            // and until the phase's date is placed between two sign events's dates.
            while (phases.length >= 2 && !(phaseIsBeforeSign() && nextPhaseIsAfterSign())) {
                phases.shift();
            }
            // at this point, phases[0] is the most recent phase with a date before the sign change date.
            // there are at least two phases before this sign (TODO can this be one?)
            log('adding moon phase update to bulk update: on ' + sign.date + ', ' + sign.sign + '-' + sign.planet + ' with phase: ' + phases[0].phase);
            bulk.find({ _id: sign._id }).updateOne({ $set: { moonPhase: phases[0].phase } });
        });
        log('execute bulk moon phase update');
        bulk.execute();
    }
    /** Generate average frequency information for `event.frequency` information. */
    processFrequencyData() {
        log('process frequency data');
        /** Store data on event frequency, indexed by a string identifying the type of event. */
        const frequency = {};
        // functions used to generate keys for 
        const keyForAspect = (event) => {
            return event.planets[0].name + '-' + event.planets[1].name + '-' + event.aspect;
        };
        const keyForSign = (event) => {
            return event.planet + '-' + event.sign;
        };
        const keyForPhase = (event) => {
            return event.planet + '-' + event.phase;
        };
        const keyForRetrograde = (event) => {
            return event.planet + '-' + (event.retrograde ? 'retrograde' : 'direct');
        };
        const differenceInSeconds = (date1, date2) => {
            const one = date1.valueOf() / 1000;
            const two = date2.valueOf() / 1000;
            return Math.abs(one - two);
        };
        /** Add one event's data to the frequency map overall calculation. */
        const addEvent = (key, event, type) => {
            if (!event.nextOccurrenceDate || !event.date) {
                // can't process an event if next occurrence and date are not set,
                // as they are the basis for calculating frequency.
                return;
            }
            const freq = differenceInSeconds(event.date, event.nextOccurrenceDate);
            if (!frequency[key]) {
                // initialize frequency for this key.
                frequency[key] = { type, minimum: null, maximum: null, mean: null, total: 0, count: 0, events: [] };
            }
            const current = frequency[key];
            current.minimum = current.minimum != null ? Math.min(current.minimum, freq) : freq;
            current.maximum = current.maximum != null ? Math.max(current.maximum, freq) : freq;
            current.count++;
            current.total += freq;
            current.mean = current.total != 0 ? current.total / current.count : null;
            current.events.push(event._id);
        };
        // go over all changes in four event types, and populate frequency map
        db.aspectchanges.find().forEach((event) => {
            if (event.exact && event.exactDates && event.exactDates.length && event.date.valueOf() == event.exactDates[0].valueOf()) {
                // only include aspect events where there is a duration and an exact aspect hit.
                addEvent(keyForAspect(event), event, 'aspect');
            }
        });
        db.signchanges.find().forEach((event) => addEvent(keyForSign(event), event, 'sign'));
        db.phasechanges.find().forEach((event) => addEvent(keyForPhase(event), event, 'phase'));
        db.retrogradechanges.find().forEach((event) => addEvent(keyForRetrograde(event), event, 'retrograde'));
        // save the frequency map
        const bulk = {
            aspect: db.aspectchanges.initializeUnorderedBulkOp(),
            sign: db.signchanges.initializeUnorderedBulkOp(),
            retrograde: db.retrogradechanges.initializeUnorderedBulkOp(),
            phase: db.phasechanges.initializeUnorderedBulkOp()
        };
        const insertFrequencies = [];
        for (const key in frequency) {
            const event = frequency[key];
            const longData = {
                event: key,
                type: event.type,
                minimum: event.minimum,
                maximum: event.maximum,
                mean: event.mean,
                count: event.count
            };
            const shortData = {
                minimum: event.minimum,
                maximum: event.maximum,
                mean: event.mean,
                count: event.count
            };
            insertFrequencies.push(longData);
            event.events.forEach((eventId) => bulk[event.type].find({ _id: eventId }).updateOne({ $set: { frequency: shortData } }));
        }
        db.averagefrequencies.remove({}); // reset table
        db.averagefrequencies.insert(insertFrequencies);
        // save the frequencies to individual events for lookup speed in applications.
        for (const key in bulk) {
            bulk[key].execute();
        }
    }
    /** Calculate and populate the aspectseries table, then the series field of each aspectchange. */
    processAspectSeries() {
        /** Bulk updates to aspectchanges. */
        const seriesList = [];
        /** Bulk insert to aspectseries collection. */
        const insertList = [];
        /** Helper function. */
        const processSeries = (events, aspect, planets) => {
            log('saving series of length ' + events.length + ' on ' + (events && events[0] ? events[0].date : " with no events "));
            const series = { _id: new ObjectId(), events: [], retrogrades: [], aspect, planets };
            events.forEach((e) => series.events.push({ event: e._id, date: e.date, longitudes: [e.planets[0].longitude, e.planets[1].longitude] }));
            insertList.push({ insert: series });
            seriesList.push({
                filter: { _id: { $in: series.events.map(e => e.event) } },
                update: { $set: { series: series._id } }
            });
        };
        /** Helper function: save all series data to database via bulk update. */
        const saveAllSeriesToDatabase = () => {
            // remove all previously populated series before beginning.
            db.aspectseries.remove({});
            log('bulk inserting ' + insertList.length + ' new aspect series');
            const insertBulk = db.aspectseries.initializeUnorderedBulkOp();
            insertList.forEach((item) => insertBulk.insert(item.insert));
            insertBulk.execute();
            log('bulk updating ' + seriesList.length + ' aspect changes (updating series property)');
            const updateBulk = db.aspectchanges.initializeUnorderedBulkOp();
            seriesList.forEach((item) => updateBulk.find(item.filter).update(item.update));
            updateBulk.execute();
            print(seriesList[0].filter._id['$in']);
            print('done updating aspect changes with series field.');
        };
        /** Read the topmost aspect on the stack, or the aspect at `depth` if set. 0 refers to the top of the stack. */
        const peekAspect = (stack, depth = 0) => {
            depth = depth || 0;
            return stack[stack.length - (depth + 1)];
        };
        /** Push an aspect onto the stack. */
        const pushAspect = (stack, aspect) => {
            if (peekAspect(stack) != aspect) {
                stack.push(aspect);
            }
        };
        /** Calculates aspect series events, inserting into aspectseries and updating aspectchanges. */
        const addAspectSeries = () => {
            log('Adding aspect series data.');
            const series = {};
            const stacks = {};
            const firstKey = (e) => e.planets[0].name + '-' + e.planets[1].name;
            const includeFields = {
                _id: true,
                aspect: true,
                entered: true,
                exact: true,
                planets: true,
                date: true
            };
            const events = db.aspectchanges.find({}, includeFields).sort({ date: 1 }).toArray(); // coalesce to avoid timeouts
            events.forEach((event) => {
                const key = firstKey(event);
                log('processing event ' + key + '-' + event.aspect);
                // prepare placement in map if necessary
                if (!stacks[key]) {
                    stacks[key] = [];
                }
                // add aspect to map if necessary
                if (!series[key]) {
                    series[key] = {};
                }
                if (!series[key][event.aspect]) {
                    series[key][event.aspect] = { events: [] };
                }
                const stack = stacks[key];
                // get the correct aspect series from the map, and push the current event
                const seri = series[key][event.aspect];
                seri.events.push(event);
                pushAspect(stack, event.aspect);
                const previousAspect = peekAspect(stack, 1);
                if (!event.applying && !event.exact && previousAspect) {
                    if (!series[key][previousAspect]) {
                        log(`error: no previous aspect for ${event.planets[0].name}-${event.aspect}-${event.planets[1].name} (${event._id} on ${event.date})`);
                        return;
                    }
                    processSeries(series[key][previousAspect].events, previousAspect, event.planets.map(p => p.name));
                    delete series[key][previousAspect];
                    stacks[key].shift();
                }
            });
            saveAllSeriesToDatabase();
        };
        /** Adds retrograde changes to aspect series. Must have events field populated first. */
        const addRetrogradeChanges = () => {
            log('Adding retrograde changes to aspect series.');
            const bulk = db.aspectseries.initializeUnorderedBulkOp();
            db.aspectseries.find({}, { _id: true, events: true }).toArray().forEach((series) => {
                if (series.events.length < 2) {
                    db.aspectseries.update({ _id: series._id }, { $set: { retrogrades: [] } });
                    return;
                }
                const events = series.events;
                const findEvents = db.aspectchanges.find({ _id: { $in: [events[0].event, events[events.length - 1].event] } }).toArray();
                const firstEvent = findEvents[0];
                const lastEvent = findEvents[1];
                const retrogradeEvents = [];
                db.retrogradechanges
                    .find({
                    date: { $gte: firstEvent.date, $lte: lastEvent.date },
                    $or: [{ planet: firstEvent.planets[0].name }, { planet: firstEvent.planets[1].name }]
                }, { retrograde: true, _id: true, date: true, planet: true })
                    .sort({ date: 1 })
                    .forEach((change) => retrogradeEvents.push({ event: change._id, date: change.date, planet: change.planet, longitude: change.longitude, retrograde: change.retrograde }));
                if (retrogradeEvents.length > 0) {
                    log('updating series on ' + firstEvent.date + ': ' + firstEvent.planets[0].name + '-' + firstEvent.planets[1].name + '-' + firstEvent.aspect + ' with ' + retrogradeEvents.length + ' retrograde events');
                    bulk.find({ _id: series._id }).updateOne({ $set: { retrogrades: retrogradeEvents } });
                }
            });
            log('executing bulk series change: retrogrades during aspect');
            bulk.execute();
        };
        // remove aspect series.
        db.aspectseries.remove({});
        db.aspectchanges.update({ series: { $ne: null } }, { $set: { series: null } }, { multi: true });
        // add series with events
        addAspectSeries();
        // add retrograde changes to series
        addRetrogradeChanges();
    }
}
///////// perform updates /////////////////////////////
const processor = new AstrologyEventMetadataProcessor();
processor.processRetrogradeFixes();
processor.processRetrogradeSigns();
processor.processAspectDates();
processor.processAspectSeries();
processor.processNextAndPrevious();
processor.processSignEventMoonPhases();
processor.processFrequencyData();
log('done populating metadata');
