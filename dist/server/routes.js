"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const handlers = require("../handlers");
/** Add routes to the express app. See ../index.ts for handler descriptions. */
exports.addRoutes = (app) => {
    // app.use(crossDomainMiddleware)
    app.get('/', handlers.information);
    app.get('/ephemeris', handlers.ephemeris);
    app.get('/sequential_events/:eventId/:type', handlers.sequential_events);
    app.get('/sequential_events', handlers.sequential_events);
    app.get('/natal_events', handlers.natal_events);
    app.get('/event/:type/:eventId', handlers.event);
    app.get('/event', handlers.event);
    app.get('/events', handlers.events);
    app.get('/events/:type', handlers.events);
    app.get('/zodiac', handlers.zodiac);
    app.get('/populate_natal', handlers.populate_natal);
};
