"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express = require("express");
exports.app = express();
const moment = require("moment");
const compression = require("compression");
exports.app.use((req, res, next) => {
    // allow any origin, not just same domain.
    res.header('Access-Control-Allow-Origin', '*');
    next();
});
exports.app.use((req, res, next) => {
    // log the request
    const startTime = moment();
    next();
    const duration = moment().diff(startTime, 'ms');
    console.log(`Request: ${req.method} (${duration}ms), IP: ${req.ip}, URL: ${req.url}`);
});
exports.app.use(compression());
