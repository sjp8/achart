"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const astrology_1 = require("../logic/astrology");
const mongoose = require("mongoose");
const signs = astrology_1.Sign.signs.map(sign => sign.name);
const planets = astrology_1.Body.bodies.map(body => body.name);
const aspects = astrology_1.Aspect.aspects.map(aspect => aspect.name);
const ObjectId = mongoose.Schema.Types.ObjectId;
const mixinDefaults = (schema) => {
    schema.type = String;
    schema.date = Date;
    schema.frequency = {
        minimum: Number, maximum: Number, mean: Number, count: Number
    };
    return schema;
};
exports.aspectChangeSchema = new mongoose.Schema(mixinDefaults({
    planets: [{
            name: String,
            longitude: Number,
            retrograde: Boolean
        }],
    aspect: String,
    major: Boolean,
    applying: Boolean,
    exact: Boolean,
    planetsSeparating: Boolean,
    inferior: Boolean,
}));
exports.aspectChangeSchema.index({ date: 1 });
exports.aspectChangeSchema.index({ aspect: 1 });
exports.aspectChangeSchema.index({ major: 1 });
exports.aspectChangeSchema.index({ exact: 1 });
exports.AspectChange = mongoose.model('AspectChange', exports.aspectChangeSchema);
exports.signChangeSchema = new mongoose.Schema(mixinDefaults({
    planet: { type: String, enum: planets },
    sign: String,
    longitude: Number,
    retrograde: Boolean
}));
exports.signChangeSchema.index({ date: 1 });
exports.signChangeSchema.index({ planet: 1 });
exports.SignChange = mongoose.model('SignChange', exports.signChangeSchema);
exports.retrogradeChangeSchema = new mongoose.Schema(mixinDefaults({
    planet: String,
    retrograde: Boolean,
    longitude: Number,
}));
exports.retrogradeChangeSchema.index({ date: 1 });
exports.retrogradeChangeSchema.index({ planet: 1 });
exports.RetrogradeChange = mongoose.model('RetrogradeChange', exports.retrogradeChangeSchema);
exports.phaseChangeSchema = new mongoose.Schema(mixinDefaults({
    planet: String,
    longitude: Number,
    sunLongitude: Number,
    distance: Number,
    phase: String,
    waxing: Boolean,
    angle: Number,
    percent: Number
}));
exports.phaseChangeSchema.index({ date: 1 });
exports.phaseChangeSchema.index({ phase: 1 });
exports.PhaseChange = mongoose.model('PhaseChange', exports.phaseChangeSchema);
exports.moonChangeSchema = new mongoose.Schema(mixinDefaults({
    event: { type: String, enum: ['perigee', 'apogee', 'voc', 'an', 'dn'] },
    longitude: Number,
    latitude: Number,
    distance: Number
}));
exports.moonChangeSchema.index({ date: 1 });
exports.moonChangeSchema.index({ type: 1 });
exports.MoonChange = mongoose.model('MoonChange', exports.moonChangeSchema);
exports.natalChangeSchema = new mongoose.Schema(mixinDefaults({
    birthDate: Number,
    latitude: Number,
    longitude: Number,
    event: { type: String, enum: ['house', 'aspect'] },
    natalPlanet: { name: String, longitude: Number, retrograde: Boolean },
    natalHouse: Number,
    transitingPlanet: { name: String, longitude: Number, retrograde: Boolean }
}));
exports.natalChangeSchema.index({ date: 1 });
exports.natalChangeSchema.index({ latitude: 1, longitude: 1 });
exports.natalChangeSchema.index({ birthDate: 1 });
exports.NatalChange = mongoose.model('NatalChange', exports.natalChangeSchema);
