# Orbital Periods
## Some notes for creating planet descriptions.

"The traditional "this is what this means" has been done before. Do something else."

* Mercury: 88 days
* Venus: 225 days
* Earth: 365 days
* Mars: 687 days
* Jupiter: 12 years
* Saturn: 29 years
* Uranus: 84 years
* Neptune: 165 years
* Pluto: 248 years
* Chiron: 50.36 years

# Major Transits

* Saturn: Opposition, Conjunction (Rudhyar)
* Uranus: Square, Opposition (Rudhyar)
* Pluto: Generational signs
* Neptune: Political or artistic movements

# What was happening when planet was discovered

## Neptune

* Neptune Was the first planet to be predicted mathematically. Perturbations in Uranus's orbit had already been observed, and using this data, Urbain Le Verrier predicted Neptune's existence with Kepler's and Newton's Laws. Le Verrier sent the calculations to the Berlin Observatory, where Johann Gottfried Galle observed Neptune within a degree of the predicted position, on the night of September 23-24, 1846.
