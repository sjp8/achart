# achart
Planetary Events REST API

## Introduction

achart creates and serves planetary events and ephemeris data via a REST API. It uses compressed data from the NASA JPL 

Populating events involves efficiently searching over periods of time, for example from January 1, 1970 to January 1, 2020 with 1-second granularity. An example of an event might be the passage of the Sun through the point of Aries, the winter solstice, the phases and nodes of the Moon, the geocentric positions of the planets, or planetary aspects.

## Notes

* `src/logic` contains wrappers around the ephemeris, and logic to calculate a variety of events.
* `src/logic/astrology/epemeris.ts` contains logic for recursive event discovery and basic ephemeris queries.
* `src/handlers/*.ts` defines the endpoint logic.
* `src/scripts` includes a command line interface to the API for populating events into the database.
* `dist/` is included for deployment to Google Cloud Functions
* See the `scripts` field in `package.json` for unit tests, and deployment instructions on localhost/Google Cloud.

## Todo

* Add unit tests for recursive event searches and check against a well known source of astrological events.
