#!/bin/bash

echo "Installing dependencies."
npm install

declare -a http=("events" "event" "sequential_events" "natal_events" "ephemeris" "zodiac" "populate_natal")
NATAL_EVENT_ENDPOINT="populate_natal_background"

CLOUDFUNCTION=$1

echo "Setting GCloud Project."
gcloud config set project astrologyapi

for i in "${http[@]}"
do
    if [ "$i" == "$CLOUDFUNCTION" ] || [ -z $CLOUDFUNCTION ] ; then
        echo "Deploying Cloud Function: $i."
        gcloud beta functions deploy $i --trigger-http --entry-point $i
    fi
done

if [ "$NATAL_EVENT_ENDPOINT" == "$CLOUDFUNCTION" ] || [ -z $CLOUDFUNCTION ] ; then
    echo "Deploying Cloud PubSub Function: $NATAL_EVENT_ENDPOINT."
    gcloud beta functions deploy $NATAL_EVENT_ENDPOINT --trigger-resource $NATAL_EVENT_ENDPOINT --trigger-event google.pubsub.topic.publish --timeout 540s --memory 2048MB
fi