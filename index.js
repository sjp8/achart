
/** Index file to expose route handlers to Google Cloud Functions. */

const handlers = require('./dist/handlers.js');
const pubsub = require('./dist/pubsub.js');

const combined = {};
[handlers, pubsub].forEach(collection => {
  Object.keys(collection).forEach(key => {
    combined[key] = collection[key];
  });
});

module.exports = combined;