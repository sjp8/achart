import * as express from 'express';
import * as moment from 'moment';
import { cors } from './cors';
import { ephemerisDatabase } from '../logic/ephemerisDatabase'
import { ephemeris as eph } from '../logic/astrology/ephemeris';

/**
 * Gets astrological events for the given time range.
 * 
 * @param{String} from The beginning of the time span in ISO 8601 compatible format.
 * @param{String} to The end of the time span in ISO 8601 compatible format.
 */
export default async function events(req: express.Request, res: express.Response) {
    cors(res)
    
    const fromDate: moment.Moment = moment.utc(req.query.from).startOf('day')
    const toDate: moment.Moment = moment.utc(req.query.to).endOf('day')
    const type: string = req.params.type || req.query.type
    const limitCount: number = parseInt(req.query.limit) ? Math.min(100, Math.max(Number(req.query.limit), 1)) : 50
    const includeOrbs: boolean = [true, 1, '1', 'true', 't', 'y', 'yes'].indexOf(req.params.includeOrbs || req.query.includeOrbs) !== -1;
  
    if (!(fromDate.isValid() && toDate.isValid() && fromDate.isBefore(toDate))) {
      return res.status(404).json({
        error: `fromDate and toDate must be valid ISO 8601-compatible date strings. fromDate must be before toDate.`
      })
    }
  
    try {
      let changes = await ephemerisDatabase.getChanges(fromDate, toDate, type)
      // apply include orbs argument
      changes = changes.filter(event => includeOrbs ? true : (event.type != 'aspect' || (event as AspectEventDocument).exact))
      // add ephemeris data to each event
      changes.forEach(event => {
        (event as any).ephemeris = eph.getPlanetsForResponse(moment.utc(event.date))
      })

      return res.json(changes);
    } catch (err) {
      return res.status(400).json({
        error: 'Error querying changes from database',
        type: 'database',
        err
      })
    }
  
  }
  