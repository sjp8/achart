import * as express from 'express';
import * as moment from 'moment';
import { cors } from './cors';
import { ephemeris as eph } from '../logic/astrology/ephemeris';
import { Body, House } from '../logic/astrology';

/** Gets an object with zodiac information for the given date and geographic coordinates. */
const getZodiacResponse = (date: moment.Moment, longitude?: number, latitude?: number) => {
    date = moment.utc(date)
    const location = {
        longitude: longitude || -122.5452863,
        latitude: latitude || 47.6497949
    }
    const planets = eph.getPlanetsArray(date)
    const planetsDisplay = planets.map(position => {
        const result: any = position.position
        result.planet = position.planet
        return result
    })
    const planetsBefore = eph.getPlanetsArray(moment.utc(date).add(-1, 'seconds'))

    const houses = eph.getHouses(date, location.longitude, location.latitude, House.houseTypes.placidus)
    const aspects = eph.getAspects(planets, planetsBefore)
    const retrograde = Body.bodies.map(planet => { return { planet: planet.name, retrograde: eph.getRetrograde(date, planet.name) } })

    const information = { display: planetsDisplay, planets, houses, aspects, retrograde }

    return information
}

/** Get the zodiac information for the given date. */
export default function zodiac(req: express.Request, res: express.Response) {
    cors(res)

    const date = moment.utc(req.query.date)
    const latitude = Number(req.query.latitude)
    const longitude = Number(req.query.longitude)
    if (!date.isValid) {
        return res.status(404).json({
            route: 'zodiac',
            error: 'Must specify a `date` parameter, an ISO 8601-compatible date string.'
        })
    }
    if (isNaN(latitude) || isNaN(longitude)) {
        return res.status(404).json({
            route: 'zodiac',
            error: 'Must specify latitude and longitude parameters, each must be valid integer or decimal numbers from -180 to 180 (longitude) or -90 to 90 (latitude).'
        })
    }
    try {
        const data = getZodiacResponse(date, longitude, latitude)
        return res.json(data)
    } catch (err) {
        return res.status(400).json({
            message: "Error retrieving zodiac data",
            err
        })
    }
}
