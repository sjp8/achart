
import informationHandler from "./information.handler";
import ephemerisHandler from "./ephemeris.handler";
import eventHandler from "./event.handler";
import eventsHandler from "./events.handler";
import zodiacHandler from "./zodiac.handler";
import populateNatalHandler from "./populatenatal.handler";
import sequentialEventsHandler from "./sequentialevents.handler";
import natalEventsHandler from "./natalevents.handler";

export const endpoints = {
  informationHandler,
  ephemerisHandler,
  eventHandler,
  eventsHandler,
  zodiacHandler,
  populateNatalHandler,
  sequentialEventsHandler,
  natalEventsHandler
};

export default endpoints;