import * as express from 'express';
import { cors } from './cors';
import { ephemerisDatabase } from '../logic/ephemerisDatabase'
import { ephemeris as eph } from '../logic/astrology/ephemeris';

/**
 * Gets an astrological event by mongo ID.
 * 
 * @param{String} The mongo ID of the event whose information to retrieve.
 */
export default async function event(req: express.Request, res: express.Response) {
    cors(res)

    const eventId = req.query.id || req.query.eventId || req.params.eventId
    const type = req.query.type || req.params.type

    const event = await ephemerisDatabase.getChangeById(eventId, type)
    try {
        return res.json({ event, eventId, type, ephemeris: eph.getPlanetsArray })
    } catch (err) {
        res.status(404).json({
            route: 'event',
            error: 'Error retrieving event.'
        })
    }
}
