import * as express from 'express';
import * as moment from 'moment';
import { cors } from './cors';
import { ephemeris as eph } from '../logic/astrology/ephemeris';

/**
 * Gets daily or hourly planetary positions for the given time span.
 * 
 * @param{String} from A standard ISO-8601 date string for the start date.
 * @param{String} to A standard ISO-8601 date string for the end date.
 * @param{String} step The interval between planetary positions (day or hour).
 */
export default function ephemeris(req: express.Request, res: express.Response) {
    cors(res)
    
    const fromDate = moment.utc(req.query.from).startOf('day')
    const toDate = moment.utc(req.query.to).endOf('day')
    const availableSteps = ['day', 'hour', 'minute', 'week', 'month', 'year']
    const includePlanets = req.query.planets ? req.query.planets.split(",") : null;
    const step = req.query.step && availableSteps.indexOf(req.query.step) !== -1 ? req.query.step : availableSteps[0]
  
    if (fromDate.isValid && toDate.isValid && step && fromDate.isBefore(toDate)) {
      const positions = eph.getPlanetPositionsOverTime(fromDate, toDate, moment.duration(1, step), includePlanets)
      return res.json(positions)
    } else {
      // date parameters are not correct
      return res.status(404).json({
        route: 'ephemeris',
        error: `fromDate and toDate are required parameters, and must both be valid ISO 8601-compatible date format strings. step, if provided, must be one of "day" or "hour".`
      })
    }
  }
  