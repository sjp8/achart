import * as express from 'express';
import * as moment from 'moment';
import { cors } from './cors';
import { ephemerisDatabase } from '../logic/ephemerisDatabase'
import { ephemeris as eph } from '../logic/astrology/ephemeris';

export default async function sequential_events(req: express.Request, res: express.Response) {
    cors(res)

    const eventId: string = req.query.eventId
    const type: string = req.params.type || req.query.type
    const countBefore: number = parseInt(req.query.countBefore) || 4
    const countAfter: number = parseInt(req.query.countAfter) || 4

    const events = await ephemerisDatabase.getSequentialChanges(eventId, type, countBefore, countAfter)
    events.forEach(event => {
        (event as any).ephemeris = eph.getPlanetsForResponse(moment.utc(event.date))
    })

    try {
        return res.json({ events }))
    } catch (err) {
        return res.status(400).json({ error: err })
    }
}
