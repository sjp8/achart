import * as express from 'express';
import { cors } from './cors';

/** Returns basic information about the app. */
export default function information(req: express.Request, res: express.Response) {
    cors(res)

    const { endpoints } = require('./index');
  
    return res.json({
      app: 'Real-Time Astrology',
      endpoints: Object.keys(endpoints)
    })
  }
  