import * as express from 'express';

/** Insert middleware directly for Google Cloud Functions support. */
export function cors(res: express.Response) {
    res.header('Access-Control-Allow-Origin', '*')
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
  }
  