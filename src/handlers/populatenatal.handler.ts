import * as express from 'express';
import * as moment from 'moment';
import * as PubSub from '@google-cloud/pubsub';
import { cors } from './cors';
import { NatalChange } from '../schema/event.schema';
import { PopulateNatalRequest } from '../pubsub';

/** HTTP handler to initiate the request to the background worker (populate_natal_background). */
export default async function populate_natal(req: express.Request, res: express.Response) {
    cors(res)

    const latitude = Number(req.query.latitude);
    const longitude = Number(req.query.longitude);
    const birthDate = moment(req.query.birthDate);

    if (!birthDate.isValid()) {
        return res.status(404).json({
            route: 'populate_natal',
            error: 'Must specify a `date` parameter, an ISO 8601-compatible date string.'
        })
    }
    if (isNaN(latitude) || isNaN(longitude)) {
        return res.status(404).json({
            route: 'populate_natal',
            error: 'Must specify latitude and longitude parameters, each must be valid integer or decimal numbers from -180 to 180 (longitude) or -90 to 90 (latitude).'
        })
    }

    let startDate = moment.utc().startOf('day').subtract(3, 'days')

    const searchCriteria = { latitude, longitude, birthDate: birthDate.unix() }
    const max = (await NatalChange.find(searchCriteria).sort({ date: 1 }).limit(1))[0]
    const min = !max ? null : (await NatalChange.find(searchCriteria).sort({ date: -1 }).limit(1))[0]

    // if the endpoint hasn't been called for this birthdate/latitude/longitude for 3 months, add another year's worth of data.
    if (min && moment(min.date).isBefore(startDate.clone().subtract(12, 'days'))) {
        console.log('add another year of natal data, the endpoint has not been called for at least 3 months.')
        startDate = moment(max.date).add(1, 'second')
    } else if (min && max) {
        return res.json({
            latitude, longitude, birthDate: birthDate.unix(), added: false
        })
    }

    const batchDuration = 4; // months

    const getBufferForBatch = (index: number): { buffer: Buffer, obj: PopulateNatalRequest } => {
        const arg: PopulateNatalRequest = {
            latitude,
            longitude,
            birthDate: birthDate.toISOString(),
            startDate: startDate.clone().add(index * batchDuration, 'months').toISOString(),
            endDate: startDate.clone().add((index + 1) * batchDuration, 'months').subtract(1, 'millisecond').toISOString()
        };

        const dataBuffer = Buffer.from(JSON.stringify(arg));

        return { buffer: dataBuffer, obj: arg };
    }

    try {
        const pubsub = PubSub();
        const batches = await Promise.all([0].map(async (batch) => {
            return await new Promise((resolve, reject) => {
                setTimeout(async () => {
                    try {
                        const buffer = getBufferForBatch(batch)
                        const id = await pubsub
                            .topic('populate_natal_background')
                            .publisher()
                            .publish(buffer.buffer)

                        resolve({ id, request: buffer.obj })
                    } catch (err) {
                        reject(err)
                    }
                }, batch * 1000);
            });
        }));

        return res.json({
            latitude, longitude, birthDate: birthDate.unix(), batches
        })
    } catch (err) {
        console.error('Error publishing populate_natal message: ', err);
        return res.status(400).json({
            error: true
        })
    }
}
