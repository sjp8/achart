import * as express from 'express';
import * as moment from 'moment';
import { cors } from './cors';
import { ephemerisDatabase } from '../logic/ephemerisDatabase'
import { ephemeris as eph } from '../logic/astrology/ephemeris';

export default async function natal_events(req: express.Request, res: express.Response) {
    cors(res)
    
    const fromDate: moment.Moment = moment.utc(req.query.from).startOf('day')
    const toDate: moment.Moment = moment.utc(req.query.to).endOf('day')
    const birthDate: moment.Moment = moment.utc(req.query.birthDate);
    const latitude = Number(req.query.latitude)
    const longitude = Number(req.query.longitude)
  
    if (isNaN(latitude) || isNaN(longitude)) {
      return res.status(404).json({
        error: `latitude and longitude are required and must be JSON-parsable numbers.`
      })
    }
  
    if (!(fromDate.isValid() && toDate.isValid() && fromDate.isBefore(toDate) && birthDate.isValid())) {
      return res.status(404).json({
        error: `fromDate and toDate must be valid ISO 8601-compatible date strings. fromDate must be before toDate.`,
        from: fromDate.toISOString(),
        to: toDate.toISOString(),
        birthDate: birthDate.toISOString()
      })
    }
  
    try {
      const events = await ephemerisDatabase.getNatalChanges(birthDate.unix(), latitude, longitude, fromDate, toDate)
      events.forEach(event => {
        (event as any).ephemeris = eph.getPlanetsForResponse(moment.utc(event.date))
      })
  
      return res.json(events)
    } catch (err) {
      return res.status(400).json({
        error: 'Error querying natal changes from database',
        type: 'database',
        obj: err
      })
    }
  }
  