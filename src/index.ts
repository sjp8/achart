
// Setup basic express app
import { app } from './server/express'

// Setup routes
import { addRoutes } from './server/routes'
addRoutes(app)

// Web service port defaults to 7444.
const port = process.env.PORT || 7444

// Start server
app.listen(port, err => {
  if (err)
    console.error(err)
  else
    console.log('listening on ' + port)
})
