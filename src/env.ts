
interface AstrologyEnvironment {
  MONGODB_URI: string;
}
const env: AstrologyEnvironment = require('../env.json');
export default env;
