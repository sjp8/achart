
import * as ephemeris from 'swisseph'
import * as TreeMap from 'treemap-js'

/** Encapsulates the definition of an aspect, e.g. what Trine means. */
export class Aspect {
  /** The name of the aspect. */
  name: string

  /** The degree value of the aspect (when it is exact). */
  angle: number

  /** Whether or not the aspect is a major aspect. */
  major: boolean

  /** The orb, or degree variation from `angle` within which the aspect is considered to be active. */
  orb: number

  /** A utf-8 string for the aspect. Null if no utf-8 character is available. */
  character?: string

  constructor(name: string, angle: number, major: boolean, orb: number, character: string) {
    this.name = name
    this.angle = angle
    this.major = major
    this.orb = orb
    this.character = character
  }

  /** Aspects and associated metadata. */
  static aspects: Aspect[] = [
    { name: 'Conjunction', major: true, angle: 0, orb: 1, character: "☌" },
    { name: 'Sextile', major: true, angle: 60, orb: 1, character: "*" },
    { name: 'Square', major: true, angle: 90, orb: 1, character: "□" },
    { name: 'Trine', major: true, angle: 120, orb: 1, character: "△" },
    { name: 'Opposition', major: true, angle: 180, orb: 1, character: "☍" },
    { name: 'Semisquare', major: false, angle: 45, orb: 1, character: 'Semisquare' },
    { name: 'Sesquisquare', major: false, angle: 135, orb: 1, character: 'Sesquisquare' },
    { name: 'Semisextile', major: false, angle: 30, orb: 1, character: 'Semisextile' },
    { name: 'Quincunx', major: false, angle: 150, orb: 1, character: 'Quincunx' },
    { name: 'Quintile', major: false, angle: 72, orb: 1, character: "Q" },
    { name: 'Biquintile', major: false, angle: 144, orb: 1, character: "bQ" },
  ]
    .filter(aspect => aspect.major)
    .map(aspect => new Aspect(aspect.name, aspect.angle, aspect.major, aspect.orb, aspect.character || null))

  static aspectsMap = new TreeMap()

  /** Gets the Aspect that fits the given angle. */
  static getAspectForAngle(angle: number): Aspect {

    return Aspect.aspects.find(aspect => Math.abs(angle - aspect.angle) < aspect.orb)
  }

  /** Calculate the angle between two longitudes. */
  static getAngleBetweenLongitudes(longitude1: number, longitude2: number): number {
    const difference = Math.max(longitude1, longitude2) - Math.min(longitude1, longitude2)
    return difference > 180 ? 360 - difference : difference
  }

  /** Gets a signed angle between longitude1 and longitude2. */
  static getDirectedAngle(longitude1: number, longitude2: number): number {
    const dot = (p1, p2) => p1.x * p2.x + p1.y * p2.y
    const cross = (p1, p2) => p1.x * p2.y - p1.y * p2.x
    const point = (angle) => {
      const angleRadians = angle * Math.PI / 180
      return {
        x: Math.cos(angle),
        y: Math.sin(angle)
      }
    }

    const points = [point(longitude1), point(longitude2)]

    return Math.atan2(cross(point[0], point[1]), dot(points[0], points[1])) * 180 / Math.PI

  }

  static getAspectForAngleBinary(angle: number): Aspect {
    const recurse = (node) => {
      if (node == null) {
        return null
      }
      const aspect = node.value
      const difference = Math.abs(aspect.angle - angle)
      const inOrb = difference <= aspect.orb
      if (inOrb) {
        return node.value as Aspect
      } else if (angle > aspect.angle) {
        return recurse(node.right)
      } else {
        return recurse(node.left)
      }
    }

    // key, value, left, right
    let map = Aspect.aspectsMap.getTree()

    return recurse(map)
  }

}

// insert into the tree, skipping around
[0, 1, 2].forEach(mod => Aspect.aspects.forEach((aspect, index) => index % 3 == mod && Aspect.aspectsMap.set(aspect.angle, aspect)))

/** Encapsulates information about a phase (illumination of a celestial body as viewed from the Earth), based on
 * a division into 8 sections of 45 degrees each.
 */
export class Phase {
  /** The name of the phase. */
  name: string

  /** The minimum degree of the phase (from 0-360 exclusive). */
  startAngle: number

  /** The maximum (from 0-360 exclusive) angle (less than, not less than or equal to). */
  endAngle: number

  /** Waxing is defined as true if the moon's illuminated portion is growing larger, false otherwise. */
  waxing: boolean

  constructor(name: string, startAngle: number, endAngle: number, waxing: boolean) {
    this.name = name
    this.startAngle = startAngle
    this.endAngle = endAngle
    this.waxing = waxing
  }

  /** Moon phases and associated metadata. */
  static phases: Phase[] = [
    { name: 'New', range: [0, 45], waxing: true },
    { name: 'Crescent', range: [45, 90], waxing: true },
    { name: 'First Quarter', range: [90, 135], waxing: true },
    { name: 'Gibbous', range: [135, 180], waxing: true },
    { name: 'Full', range: [180, 225], waxing: false },
    { name: 'Disseminating', range: [225, 270], waxing: false },
    { name: 'Third Quarter', range: [270, 315], waxing: false },
    { name: 'Balsamic', range: [315, 360], waxing: false }
  ].map(phase => new Phase(phase.name, phase.range[0], phase.range[1], phase.waxing))

  /** Gets a full angle from angle and waxing status. */
  static getFullAngle(angle: number, waxing: boolean): number {
    return waxing ? angle : 180 + (180 - angle)
  }

  /** Get the Phase for the given angle and waxing status. */
  static getPhase(fullAngle: number): Phase {
    return Phase.phases.find(phase => fullAngle >= phase.startAngle && fullAngle < phase.endAngle)
  }

}

/** Encapsulates information about a celestial body. */
export class Body {

  /** The name of the planet, comet, or celestial body. */
  name: string

  /** A utf-8 string for the celestial body. */
  character: string

  /** The swiss ephemeris constant for the celestial body. */
  constant: any

  constructor(name: string, character: string, constant: any) {
    this.name = name
    this.character = character
    this.constant = constant
  }

  /** Planetary and other celestial bodies and associated metadata. */
  static bodies = [
    { type: 'Planet', constant: ephemeris.SE_SUN, name: 'Sun', character: "☉" },
    { type: 'Planet', constant: ephemeris.SE_MOON, name: 'Moon', character: "☽" },
    { type: 'Planet', constant: ephemeris.SE_MERCURY, name: 'Mercury', character: "☿" },
    { type: 'Planet', constant: ephemeris.SE_MARS, name: 'Mars', character: "♂" },
    { type: 'Planet', constant: ephemeris.SE_VENUS, name: 'Venus', character: "♀" },
    { type: 'Planet', constant: ephemeris.SE_JUPITER, name: 'Jupiter', character: "♃" },
    { type: 'Planet', constant: ephemeris.SE_SATURN, name: 'Saturn', character: "♄" },
    { type: 'Planet', constant: ephemeris.SE_URANUS, name: 'Uranus', character: "♅" },
    { type: 'Planet', constant: ephemeris.SE_NEPTUNE, name: 'Neptune', character: "♆" },
    { type: 'Planet', constant: ephemeris.SE_PLUTO, name: 'Pluto', character: "♇" },
    { type: 'Planet', constant: ephemeris.SE_CHIRON, name: 'Chiron', character: null }
  ].map(body => new Body(body.name, body.character, body.constant))
}

/** Encapsulates a sign of the zodiac. */
export class Sign {
  /** The name of the sign. */
  name: string

  /** The starting degree of the sign. */
  degree: number

  /** The 0-based index of the sign. */
  index: number

  /** The utf-8 character of the sign. */
  character: string

  constructor(name: string, degree: number, index: number, character: string) {
    this.name = name
    this.degree = degree
    this.index = index
    this.character = character
  }

  /** Signs and associated metadata. */
  static signs = [
    { name: 'Aries', degree: 0, number: 1, character: "♈" },
    { name: 'Taurus', degree: 30, number: 2, character: "♉" },
    { name: 'Gemini', degree: 60, number: 3, character: "♊" },
    { name: 'Cancer', degree: 90, number: 4, character: "♋" },
    { name: 'Leo', degree: 120, number: 5, character: "♌" },
    { name: 'Virgo', degree: 150, number: 6, character: "♍" },
    { name: 'Libra', degree: 180, number: 7, character: "♎" },
    { name: 'Scorpio', degree: 210, number: 8, character: "♏" },
    { name: 'Sagittarius', degree: 240, number: 9, character: "♐" },
    { name: 'Capricorn', degree: 270, number: 10, character: "♑" },
    { name: 'Aquarius', degree: 300, number: 11, character: "♒" },
    { name: 'Pisces', degree: 330, number: 12, character: "♓" },
  ].map((sign, index) => new Sign(sign.name, sign.degree, index, sign.character))

  static getSignAndDegree(longitude: number): { sign: Sign, degree: number, minute: number, second: number, longitude: number } {
    // how many times does 30 go into the planet longitude?
    // example: 93.141352345 (should be 3 degrees Cancer, 8 minutes, 28 seconds)
    const signIndex = Math.floor(longitude / 30)
    const sign = Sign.signs[signIndex]

    const degreeDecimal = longitude - signIndex * 30
    const degree = Math.floor(degreeDecimal)

    const minutesDecimal = (degreeDecimal - degree) * 60
    const minute = Math.floor(minutesDecimal)
    const second = Math.floor((minutesDecimal - minute) * 60)

    return { sign, degree, minute, second, longitude }
  }
}

export class House {

  /** House systems. */
  static houseTypes = {
    placidus: 'P',
    koch: 'K',
    porphyrius: 'O',
    regiomontanus: 'R',
    campanus: 'C',
    equal: 'A',
    wholeSign: 'W'
  }
}
