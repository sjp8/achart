
import test from 'ava';

import { Ephemeris } from './ephemeris'
import { Body } from './types'
import * as swisseph from 'swisseph'
import * as moment from 'moment'

let lastEphemeris
const getEphemeris = (): Ephemeris => lastEphemeris || new Ephemeris(Ephemeris.dataSources.swisseph, swisseph)

test('ephemeris loads and can calculate planetary positions', t => {
  const ephemeris = getEphemeris()
  let now = moment()
  let before = moment().add(-1, 'minutes')
  let sun = ephemeris.getPlanet(now, 'Sun')
  let moon = ephemeris.getPlanet(now, 'Moon')
  let sun0 = ephemeris.getPlanet(before, 'Sun')
  let moon0 = ephemeris.getPlanet(before, 'Moon')

  t.truthy(sun)
  t.truthy(ephemeris.getAspect(sun, moon, sun0, moon0))
  t.truthy(ephemeris.getHouses(now, 0, 30))
  let moonR = ephemeris.getRetrograde(now, 'Sun')
  t.not(moonR, null)
  t.skip.false(moonR) // moon should never be retrograde
})

test('speed of aspect calculation', t => {
  const ephemeris = getEphemeris()
  const now = moment()

  // ephemeris.
})