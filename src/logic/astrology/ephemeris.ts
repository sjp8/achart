
import * as moment from 'moment'
import * as _ from 'underscore'
import * as path from 'path'
import * as swisseph from 'swisseph'

import {
  Aspect, Body,
  PlanetaryEphemerisData, PlanetaryPosition, MoonPhase, PlanetaryAspect, HousePosition,
  ChangeEvent, PhaseChangeEvent, SignChangeEvent, RetrogradeChangeEvent, AspectChangeEvent, MoonChangeEvent, NatalTransitEvent
} from './'

declare var __dirname;

/**
 * Methods to query swiss ephemeris based on compressed NASA JPL files.
 */
export class Ephemeris {

  /** Available data sources from which to query astrological data. */
  static dataSources = {
    jpl: swisseph.SEFLG_JPLEPH,
    swisseph: swisseph.SEFLG_SWIEPH
  }

  /** The ephemeris files directory. */
  static ephemerisFiles = path.join(__dirname, '../../../ephemeris_files')

  /** A reference to the swisseph library. */
  ephemeris: any // the swisseph library

  /** The current data source (see dataSources) */
  dataSource: string

  /** Get the data source enum value*/
  get dataSourceValue(): number { return Ephemeris.dataSources[this.dataSource] }

  /** The default log date format */
  dateLogFormat: string = 'YYYY-MM-DD h:mm:ss a'

  /** The sequence of time intervals on which to perform searches for event changes. */
  recursiveSearchSeries = [24 * 60 * 60, 60 * 60, 30 * 60, 10 * 60, 60, 10, 1]

  /** The minimum search unit granularity (choose seconds or minutes). */
  recursiveSearchUnit: any = 'second'

  constructor(dataSource: string, swissEphemeris: any) {
    this.dataSource = dataSource
    this.ephemeris = swissEphemeris

    this.load()
  }

  /** Setup the data source (if necessary). */
  private load() {
    this.ephemeris.dataSource = Ephemeris.dataSources[this.dataSource]
    this.ephemeris.swe_set_ephe_path(Ephemeris.ephemerisFiles)
  }

  /** Gets the position of a planet at the given date and time. */
  getPlanet(date: moment.Moment, planet: string): PlanetaryPosition {
    const julian = Ephemeris.getJulianTime(date)
    const planetaryConstant = this.ephemeris['SE_' + planet.toUpperCase()]
    const body = this.ephemeris.swe_calc_ut(julian, planetaryConstant, this.dataSourceValue | this.ephemeris.SEFLG_SPEED)
    if (body.error) {
      console.error(body.error)
      return null
    }
    return new PlanetaryPosition(date, planet, body)
  }

  /** Gets the position of all the planets including Chiron (see Body.bodies), for the given date and time. */
  getPlanets(date: moment.Moment): { [key: string]: PlanetaryPosition } {
    let planets = {}

    Body.bodies.forEach((body) => {
      const position = this.getPlanet(date, body.name)
      planets[body.name] = position
    })

    return planets
  }

  getPlanetsForResponse(date: moment.Moment): { planets: string[], longitudes: number[], latitudes: number[], distances: number[], longitudeSpeeds: number[] } {
    const planets = this.getPlanetsArray(date);
    const planetStrings = planets.map(p => p.planet);
    const longitudes = planets.map(p => p.longitude);
    const latitudes = planets.map(p => p.latitude);
    const longitudeSpeeds = planets.map(p => p.longitudeSpeed);
    const distances = planets.map(p => p.distance);

    return { planets: planetStrings, longitudes, latitudes, longitudeSpeeds, distances };
  }

  getPlanetsArray(date: moment.Moment): PlanetaryPosition[] {
    return Body.bodies.map(body => this.getPlanet(date, body.name))
  }

  /** Gets the phase of the moon for the given date and time. */
  getMoonPhase(date: moment.Moment): MoonPhase {
    const sun = this.getPlanet(date, 'Sun')
    const moon = this.getPlanet(date, 'Moon')

    const angle = Aspect.getAngleBetweenLongitudes(sun.longitude, moon.longitude)
    // waxing if they are separating, because the larger the angle, the closer to full moon.
    const waxing = PlanetaryAspect.planetsAreSeparating(sun, moon)

    return new MoonPhase(date, angle, waxing, moon.longitude, moon.distance, sun.longitude)
  }

  /** Convert date parts (year, month, day, hour, minute, second) to a julian time number. */
  static getJulianTimeFromParts(parts: { [dateParts: string]: number }): number {
    const julian = swisseph.swe_utc_to_jd(parts.year, parts.month, parts.day, parts.hour, parts.minute, parts.second, swisseph.SE_GREG_CAL)
    if (julian.error) {
      console.error('Error in getJulianTime: ' + julian.error)
      return null
    } else {
      return julian.julianDayUT
    }
  }

  /** Get the julian time number from a date. Formerly getJulianTimeFromDate. */
  static getJulianTime(date: moment.Moment): number {
    return Ephemeris.getJulianTimeFromParts(Ephemeris.getDateParts(date))
  }

  /**
   * Get parts of a date (year, month, day, hour, minute, second) from a date.
   * Formerly getDataFromMoment.
   */
  static getDateParts(date: moment.Moment): { [datePart: string]: number } {
    return {
      year: date.year(),
      month: date.month() + 1,
      day: date.date(),
      hour: date.hour(),
      minute: date.minute(),
      second: date.seconds()
    }
  }

  /** Gets the planetary aspect between the two planets, using their before positions to determine the exactness of the aspect. */
  getAspect(planet1: PlanetaryPosition, planet2: PlanetaryPosition, planet1Before: PlanetaryPosition, planet2Before: PlanetaryPosition): PlanetaryAspect {
    const before = new PlanetaryAspect(planet1Before.date, planet1Before, planet2Before, false)
    const after = new PlanetaryAspect(planet1.date, planet1, planet2, false)
    const aspect = after.aspect

    const isExactOpposition = aspect && aspect.angle === 180 && before.applying && !after.applying
    // const isExactConjunction = aspect && aspect.angle === 0 && PlanetaryAspect.isAtConjunction(planet1.longitude, planet1.longitudeSpeed, planet2.longitude, planet2.longitudeSpeed);
    const isExactConjunction = aspect && aspect.angle === 0 && before.applying && !after.applying
    const exact = aspect && (PlanetaryAspect.isCrossingOverAngle(before.angle, after.angle, aspect.angle) || isExactConjunction || isExactOpposition);

    return new PlanetaryAspect(planet1.date, planet1, planet2, exact)
  }

  /**
   * Gets all current active aspects between all the planets, populating
   * exactness and entering/leaving status. 
   */
  getAspects(planets: PlanetaryPosition[], planetsBefore: PlanetaryPosition[]): PlanetaryAspect[] {
    let aspects: PlanetaryAspect[] = []

    planets.forEach((planet1, index1) => {
      planets.forEach((planet2, index2) => {
        if (planet1.planet >= planet2.planet) {
          return // alphabetical
        }

        const aspect = this.getAspect(planet1, planet2, planetsBefore[index1], planetsBefore[index2])
        aspects.push(aspect)
      })
    })
    return aspects
  }

  /** Gets all changed aspects between the transiting planets (in the past, and now) and the given static natal planets. */
  getChangedNatalAspects(planets: PlanetaryPosition[], planetsBefore: PlanetaryPosition[], natalPlanets: PlanetaryPosition[]): PlanetaryAspect[] {
    const changedAspects: PlanetaryAspect[] = [];

    planets.forEach((transit, transitIndex) => {
      natalPlanets.forEach((natal, natalIndex) => {
        const afterAspect = this.getAspect(transit, natal, planetsBefore[transitIndex], natal);
        const beforeAspect = new PlanetaryAspect(planetsBefore[transitIndex].date, planetsBefore[transitIndex], natal);
        const exact = afterAspect.exact;
        const newAspect = !beforeAspect.hasAspect && afterAspect.hasAspect;
        const fadedAspect = beforeAspect.hasAspect && !afterAspect.hasAspect;

        if (exact || newAspect) {
          changedAspects.push(afterAspect);
        } else if (fadedAspect) {
          changedAspects.push(beforeAspect);
        }
      })
    })

    return changedAspects;
  }

  /**
   * Gets all changed aspects (in the last second) between all the planets, using the before 
   * positions of the planets to determine exactness and entering/leaving status.
   */
  getChangedAspects(planets: PlanetaryPosition[], planetsBefore: PlanetaryPosition[]): PlanetaryAspect[] {
    let changedAspects: PlanetaryAspect[] = []
    
    // new version:
    // * goes through planets, calculates aspects (optimizes by only checking aspects that are likely... might not work
    // with recursive case, or even because things are not necessarily done in order.
    // * 

    planets.forEach((planet1, index1) => {
      planets.forEach((planet2, index2) => {
        if (planet1.planet >= planet2.planet) {
          return // alphabetical
        }

        const afterAspect = this.getAspect(planet1, planet2, planetsBefore[index1], planetsBefore[index2])
        const beforeAspect = new PlanetaryAspect(planetsBefore[index1].date, planetsBefore[index1], planetsBefore[index2], false)

        // we have an exact aspect (it is the closest possible angle, by the second, to the exact aspect angle).
        const exact = afterAspect.exact
        // we have an entering aspect (no aspect before, aspect now).
        const newAspect = !beforeAspect.hasAspect && afterAspect.hasAspect
        // we have a leaving aspect (it was there before, now it's gone).
        const fadedAspect = beforeAspect.hasAspect && !afterAspect.hasAspect

        if (exact || newAspect) {
          changedAspects.push(afterAspect)
        } else if (fadedAspect) {
          changedAspects.push(beforeAspect)
        }
      })
    })
    return changedAspects
  }

  /** Gets the houses for the given date, at the given location on the surface of the Earth. */
  getHouses(date: moment.Moment, longitude: number, latitude: number, houseSystem: string = 'P'): HousePosition[] {
    const julian = Ephemeris.getJulianTime(date)
    console.log(`${julian}, ${latitude}, ${longitude}`);
    const houses = this.ephemeris.swe_houses(julian, latitude, longitude, houseSystem)
    if (houses.error) {
      console.error(houses.error)
      return
    }

    let result: HousePosition[] = []
    for (let i = 0; i < houses.house.length; i++) {
      const position = new HousePosition(date, i + 1, houses.house[i], houses.house[(i + 1) % 12])
      result.push(position)
    }

    return result
  }

  /** Given an array of 12 houses, returns the appropriate house for the given longitude. */
  getHouseForLongitude(houses: HousePosition[], longitude: number): HousePosition {
    return houses.find((house, houseIndex) => longitude >= house.cusp && (longitude < house.end || house.number === 12))
  }

  /** Gets the retrograde status for the given date and planet. */
  getRetrograde(date: moment.Moment, planet: string): boolean {
    const lastSecond = moment.utc(date).add(-1, 'seconds')
    const position = this.getPlanet(date, planet)
    const positionBefore = this.getPlanet(lastSecond, planet)
    return position.longitude < positionBefore.longitude
  }

  /**
   * A generic function for searching planetary changes over a period of time.
   * 
   * @param beginDate Search from this date.
   * @param endDate Search until this date.
   * @param changeFunction Function to compute changes between two objects retrieved by `getFunction`.
   * @param getFunction Function to retrieve an object given a date.
   * @param onEventFunction Callback to call for any change event found by the search.
   * @param logFunction Callback to call for any change event found by the search, used to log the change.
   */
  searchPlanetaryChanges<P extends PlanetaryEphemerisData, E extends ChangeEvent>(
      beginDate: moment.Moment, // search from this date
      endDate: moment.Moment, // to this date
      changeFunction: { (previous: P[], current: P[]): E[] }, // function to compute changes between two time-consecutive getFunction results
      getFunction: { (date: moment.Moment): P[] },    // function to get data for the given date (defaults to planets positions at date)
      onEventFunction: { (change: E): void }, // callback function when a change has been found
      onFinishedFunction: { (): void }, // callback function when all changes between beginDate and endDate have been returned
      logFunction?: { (change: E): void },      // callback function to log a change
      options?: RecursiveSearchOptions
    )
  {
    // search starts off with a wide sweep (see Ephemeris.recursiveSearchSeries)
    // honing in on time sections with changes.

    options = options || {
      minimumUnit: this.recursiveSearchUnit,
      recursiveSearchSeries: this.recursiveSearchSeries
    }

    /**
     * Gets an array of changes for the given time period.
     * 
     * @param{moment.Moment} startDate The beginning of the period to check for changes.
     * @param{number} forSeconds How far from `startDate` to check for changes.
     * @param{P[]} previous The previous <P> array, computed for startDate. If null, queries <P> with `getFunction(startDate)`.
     */
    let getChangesForPeriod = (startDate: moment.Moment, forSeconds: number): { changes: E[], current: P[], previous: P[] } => {
      // calculate the end date based on start date and the duration of the period.
      const endDate = moment.utc(startDate).add(forSeconds, options.minimumUnit)

      // if previous is null, e.g. the first time this method is called, then compute it from startDate.
      const previous = getFunction(startDate) || []

      // get the P[] for endDate (it hasn't been computed yet)
      const current = getFunction(endDate) || []

      // perform the change function on the previous and current changes.
      const changes = changeFunction(previous, current) || []

      return {
        changes,
        current, // the "previous" P[] is now the latest P[]
        previous
      }
    }

    // calculate the number of seconds between beginDate and endDate
    const numberTimeUnits = moment.utc(endDate).diff(beginDate, options.minimumUnit)
    const date = moment.utc(beginDate)
    let recurseCount = 0

    // track the planetary changes (or other object from `getFunction`) for use in comparing
    // current vs. previous, and detecting which statuses changed
    let previous: P[] = null

    /**
     * Find changes, calling `onEventFunction` and `logFunction` for each.
     * 
     * @param startDate Starting at this date.
     * @param depth The 0-indexed depth, corresponding to Ephemeris.planetarySearchSeries.
     * @param forSeconds The length of time to look for changes
     */
    const recurse = (startDate: moment.Moment, depth: number, forSeconds: number) => {
      recurseCount += 1 // keep track of recurse calls to determine when recursion is finished.

      const step = options.recursiveSearchSeries[depth]
      const isLastSearchSeries = depth == options.recursiveSearchSeries.length - 1

      for (let m = 0; m < forSeconds; m += step) {
        //console.log('processing chunk for ' + startDate.calendar() + ' for step ' + step)

        // get changes for this time series, starting at startDate, sending cached `previous`
        // PlanetaryPosition, etc array to the computation for changes.
        const chunk = getChangesForPeriod(startDate, step)

        // if there were any changes in this time period, recurse or add them as necessary
        if (chunk.changes.length) {
          if (isLastSearchSeries) {
            chunk.changes.forEach((change, index) => {
              if (onEventFunction) {
                onEventFunction(change)
              }
              if (logFunction) {
                logFunction(change)
              }
            })
          } else {
            // if there were changes, and there is more 
            //console.log('recurse into more granular search step: ' + options.recursiveSearchSeries[depth + 1])
            recurse(moment.utc(startDate.valueOf()), depth + 1, step)
          }
        }

        // continue to the next chunk at the current depth by adding `step` to startDate and
        // processing the next chunk for changes
        startDate.add(step, options.minimumUnit)
      }

      recurseCount -= 1
      if (recurseCount === 0) {
        onFinishedFunction()
      }
    }

    process.nextTick(() => {
      recurse(moment.utc(beginDate.valueOf()), 0, numberTimeUnits)
    })
  }

  searchMoonPhaseChanges(beginDate: moment.Moment, endDate: moment.Moment, onEvent: { (change: PhaseChangeEvent): void }, onFinished: { (): void }): void {
    this.searchPlanetaryChanges<MoonPhase, PhaseChangeEvent>(
      beginDate,
      endDate,
      (previous: MoonPhase[], current: MoonPhase[]): PhaseChangeEvent[] => {
        if (previous && previous[0] && current && current[0] && current[0].phase.name != previous[0].phase.name) {
          const prev = previous[0]
          const curr = current[0]
          return [new PhaseChangeEvent(current[0].date, current[0])]
        } else {
          return []
        }
      },
      (date: moment.Moment): MoonPhase[] => {
        return [this.getMoonPhase(date)]
      },
      (change: PhaseChangeEvent) => onEvent(change),
      onFinished,
      (change: PhaseChangeEvent) => console.log(change.date.calendar() + ': ' + (change.phase.phase.waxing ? ' Waxing ' : ' Waning ') + change.phase.phase.name + ' ' + change.phase.fullAngle)
    )
  }

  searchMoonEventChanges(beginDate: moment.Moment, endDate: moment.Moment, onEvent: { (change: MoonChangeEvent): void }, onFinished: { (): void }): void {
    this.searchPlanetaryChanges<PlanetaryPosition, MoonChangeEvent>(
      beginDate,
      endDate,
      (previous: PlanetaryPosition[], current: PlanetaryPosition[]): MoonChangeEvent[] => {
        const p = previous[0], c = current[0]
        const apogee = p.distanceSpeed > 0 && c.distanceSpeed <= 0 // was moving away from Earth, now moving back towards.
        const perigee = p.distanceSpeed <= 0 && c.distanceSpeed > 0 // was moving towards the Earth, now moving away again.
        const an = p.latitude < 0 && c.latitude >= 0 // the moon was below the eliptic, and is now above.
        const dn = c.latitude <= 0 && p.latitude > 0 // the moon was above the ecliptic, and now is below.
        if (apogee || perigee || an || dn) {
          return [new MoonChangeEvent(c.date, c, perigee, apogee, an, dn)]
        }
      },
      (date: moment.Moment): PlanetaryPosition[] => {
        return [this.getPlanet(date, 'Moon')]
      },
      (change: MoonChangeEvent) => onEvent(change),
      onFinished,
      (change: MoonChangeEvent) => console.log(`${change.date.calendar()}: ${change.types.join(', ')}, latitude: ${change.moon.latitude}, distance (AU): ${change.moon.distance}, distanceSpeed: ${change.moon.distanceSpeed}`)
    )
  }

  searchSignChanges(beginDate: moment.Moment, endDate: moment.Moment, onEvent: { (change: SignChangeEvent): void }, onFinished: { (): void }): void {
    this.searchPlanetaryChanges<PlanetaryPosition, SignChangeEvent>(
      beginDate,
      endDate,
      (previous: PlanetaryPosition[], current: PlanetaryPosition[]): SignChangeEvent[] => {
        const p: { [planet: string]: PlanetaryPosition } = {}, c: { [planet: string]: PlanetaryPosition } = {}
        previous.forEach(prev => prev && prev.planet ? p[prev.planet] = prev : null)
        current.forEach(curr => curr && curr.planet ? c[curr.planet] = curr : null)
        const planets = Object.keys(p)
        const result: SignChangeEvent[] = []

        planets.forEach((planet) => {
          if (p[planet].sign.name != c[planet].sign.name) {
            result.push(new SignChangeEvent(c[planet].date, c[planet], p[planet]))
          }
        })

        return result
      },
      (date: moment.Moment): PlanetaryPosition[] => {
        const planets = this.getPlanetsArray(date)
        return planets
      },
      (change: SignChangeEvent) => onEvent(change),
      onFinished,
      (change: SignChangeEvent) => console.log(change.date.calendar() + ': ' + change.planet.planet + ' ' + change.planet.sign.name + ' ' + change.planet.longitude)
    )
  }

  searchAspectChanges(beginDate: moment.Moment, endDate: moment.Moment, onEvent: { (change: AspectChangeEvent): void }, onFinished: { (): void }): void {
    console.log('aspect changes between ' + beginDate.calendar() + ' and ' + endDate.calendar())

    this.searchPlanetaryChanges<PlanetaryPosition, AspectChangeEvent>(
      beginDate,
      endDate,
      (previous: PlanetaryPosition[], current: PlanetaryPosition[]): AspectChangeEvent[] => {
        const changed = this.getChangedAspects(current, previous)
        const events = changed.map(aspect => new AspectChangeEvent(aspect.date, aspect))
        return events
      },
      (date: moment.Moment): PlanetaryPosition[] => {
        return this.getPlanetsArray(date)
      },
      (change: AspectChangeEvent) => onEvent(change),
      onFinished,
      (change: AspectChangeEvent) => console.log(`${change.date.calendar()}: ${change.aspect.planet1.planet}-${change.aspect.aspect.name}-${change.aspect.planet2.planet}-${change.aspect.exact}-${change.aspect.applying}`)
    )
  }

  searchNatalChanges(birthDate: number, latitude: number, longitude: number, natalPositions: PlanetaryPosition[], natalHouses: HousePosition[], beginDate: moment.Moment, endDate: moment.Moment, onEvent: { (change: NatalTransitEvent) }, onFinished: { () }, options?: RecursiveSearchOptions) {
    this.searchPlanetaryChanges<PlanetaryPosition, NatalTransitEvent>(
      beginDate,
      endDate,
      (previous: PlanetaryPosition[], current: PlanetaryPosition[]): NatalTransitEvent[] => {
        const results = [];

        // house changes
        // - for each planet, find out its house beforehand, and after.
        // - for the houses that have changed, add an event
        const getPlanetsToHouseIndex = (positions: PlanetaryPosition[], housePositions: HousePosition[]): { [planet: string]: HousePosition } => {
          const result = {};
          positions.forEach(p => {
            // planet is between this house cusp, and the next. But if the next cusp is lower than this one (it crosses over 0), then
            // the planet should be either greater than the lower, or less than the higher index house cusp.
            result[p.planet] = housePositions.find((h, i) =>  {
              const houses = [housePositions[i], housePositions[(i + 1) % 12]];
              if (houses[0].cusp > houses[1].cusp) {
                return p.longitude >= houses[0].cusp || p.longitude < houses[1].cusp;
              } else {
                return houses[0].cusp <= p.longitude && houses[1].cusp > p.longitude;
              }
            })
          })

          return result;
        }
        const housesBefore = getPlanetsToHouseIndex(previous, natalHouses);
        const housesAfter = getPlanetsToHouseIndex(current, natalHouses);
        // const changedHouses: { date: moment.Moment, planet: string, house: number }[] = [];
        current.forEach(p => {
          if (housesBefore[p.planet] != housesAfter[p.planet]) {
            results.push(new NatalTransitEvent(p.date, 'house', birthDate, latitude, longitude, {
              transitingPlanet: p,
              natalHouse: housesAfter[p.planet].index + 1
            }));
          }
        })

        // aspects between any natal and any transiting planet
        const changedAspects = this.getChangedNatalAspects(current, previous, natalPositions);

        changedAspects.forEach(aspect => {
          aspect.exact && results.push(new NatalTransitEvent(aspect.date, 'aspect', birthDate, latitude, longitude, {
            natalPlanet: aspect.planet2,
            transitingPlanet: aspect.planet1
          }));
        });

        return results;
      },
      (date: moment.Moment): PlanetaryPosition[] => {
        return this.getPlanetsArray(date)
      },
      onEvent,
      onFinished,
      change => change.natalHouse ? console.log(`natal house change: ${change.date.calendar()}: ${change.transitingPlanet.planet} ${change.natalHouse}`) : console.log(`natal aspect change: ${change.date.calendar()}: ${change.transitingPlanet.planet} ${change.natalPlanet.planet}`)
    );
  }

  searchRetrogradeChanges(beginDate: moment.Moment, endDate: moment.Moment, onEvent: { (change: RetrogradeChangeEvent): void }, onFinished: { (): void }): void {
    this.searchPlanetaryChanges<PlanetaryPosition, RetrogradeChangeEvent>(
      beginDate,
      endDate,
      (previous: PlanetaryPosition[], current: PlanetaryPosition[]): RetrogradeChangeEvent[] => {
        return current
          .filter((c, index) => previous[index] && c.retrograde !== previous[index].retrograde)
          .map(planet => new RetrogradeChangeEvent(planet.date, planet))
      },
      (date: moment.Moment): PlanetaryPosition[] => {
        const planets = this.getPlanetsArray(date)
        return planets
      },
      (change: RetrogradeChangeEvent) => onEvent(change),
      onFinished,
      (change: RetrogradeChangeEvent) => console.log(`${change.date.calendar()}: ${change.planet.planet} ${change.planet.retrograde ? ' retrograde' : ' direct'}`)
    )
  }

  getPlanetPositionsOverTime(startDate: moment.Moment, endDate: moment.Moment, step: moment.Duration, includePlanets: string[] = null): PositionsOverTime {

    // a list of planets to include
    const planetNames = includePlanets ? includePlanets.filter(p => Body.bodies.find(b => b.name === p)) : Body.bodies.map(b => b.name);

    const result: PositionsOverTime = {
      fields: ['longitude', 'latitude', 'distance', 'longitudeSpeed', 'latitudeSpeed', 'retrograde'],
      planets: planetNames,
      times: []
    };
    for (let date: moment.Moment = moment.utc(startDate); date.isBefore(endDate); date.add(step)) {
      const planets = planetNames.map(planet => this.getPlanet(date, planet));
      const rows = planets.map(planet => {
        return [planet.longitude, planet.latitude, planet.distance, planet.longitudeSpeed, planet.latitudeSpeed, planet.retrograde]
      })
      result.times.push({
        date: date.valueOf(),
        planets: rows
      });
    }

    return result
  }
}

export class PositionsOverTime {
  fields: string[]
  planets: string[]
  times: {
    date: number,
    planets: (number | boolean)[][]
  }[]
}

interface RecursiveSearchOptions {
  minimumUnit: any
  recursiveSearchSeries: number[]
}

export const ephemeris = new Ephemeris(Ephemeris.dataSources.swisseph, swisseph);
