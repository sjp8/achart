
import * as moment from 'moment'

import { Phase, Sign, Aspect } from './types'
import { Ephemeris } from './ephemeris'

/** An abstract class to encapsulate any data coming from the ephemeris. */
export class PlanetaryEphemerisData {
  /** The UTC date for this data. */
  date: moment.Moment

  constructor(date: moment.Moment) {
    this.date = moment.utc(date)
  }

}

/** A moon phase event. */
export class MoonPhase extends PlanetaryEphemerisData {

  /** The name of the moon phase, from Astrology.phases. */
  phase: Phase

  /** The angle: a number between 0 and 180 degrees. Use in conjunction with `waxing` to get the moon phase. */
  angle: number

  /** The full angle is a number between 0 and 360 degrees. */
  get fullAngle() {
    return Phase.getFullAngle(this.angle, this.waxing)
  }

  /** True if the moon is growing more illuminated. False if it is becoming less illuminated. */
  waxing: boolean

  planet: string = 'Moon'

  /** The longitude at which the moon phase occurred. */
  longitude: number

  /** The longitude of the sun during the phase. */
  sunLongitude: number

  /** The current distance from the Earth measured in AU. */
  distance: number

  constructor(date: moment.Moment, angle: number, waxing: boolean, longitude: number, distance: number, sunLongitude: number) {
    super(date)
    this.waxing = waxing
    this.angle = angle
    this.phase = Phase.getPhase(this.fullAngle)
    this.longitude = longitude
    this.sunLongitude = sunLongitude
    this.distance = distance
  }

  /** Gets the angle parts (angle less than 180 degrees, and waxing status) from the full angle. */
  getAngle(fullAngle: number): { angle: number, waxing: boolean } {
    if (fullAngle >= 180) {
      return { angle: 360 - fullAngle, waxing: false }
    } else {
      return { angle: fullAngle, waxing: true }
    }
  }
}

/** Encapsulates the planetary position at the given time. */
export class PlanetaryPosition extends PlanetaryEphemerisData {
  /** The planet. */
  planet: string

  /** Longitude (e.g. degrees from 0 Aries). */
  longitude: number
  /** Latitude (e.g. degrees above ecliptic). */
  latitude: number
  /** Delta longitude (per day). */
  longitudeSpeed: number
  /** Delta latitude (per day). */
  latitudeSpeed: number
  /** Distance from Earth (measured in AU). */
  distance: number
  /** Delta distance (per day). */
  distanceSpeed: number

  /** Retrograde status. */
  get retrograde() { return this.longitudeSpeed < 0 }

  /** Sign */
  get sign(): Sign {
    const signIndex = Math.floor(this.longitude / 30)
    return Sign.signs[signIndex]
  }

  get position(): { sign: Sign, degree: number, minute: number, second: number } {
    return Sign.getSignAndDegree(this.longitude)
  }

  constructor(date: moment.Moment, planet: string, data: any) {
    super(date)

    this.planet = planet
    this.longitude = data.longitude
    this.latitude = data.latitude
    this.longitudeSpeed = data.longitudeSpeed
    this.latitudeSpeed = data.latitudeSpeed
    this.distance = data.distance
    this.distanceSpeed = data.distanceSpeed
  }

  /** Shortcut for Ephemeris.getJulianTime */
  get julianTime(): number { return Ephemeris.getJulianTime(this.date) }

  /** Shortcut for Ephemeris.getDateParts */
  get dateParts(): { [datePart: string]: number } { return Ephemeris.getDateParts(this.date) }

}

/** Information about an aspect formed by two planets at a particular time. */
export class PlanetaryAspect extends PlanetaryEphemerisData {
  /** The first planet of the aspect. */
  planet1: PlanetaryPosition

  /** The second planet forming the aspect. */
  planet2: PlanetaryPosition

  /** True if the aspect is exact, i.e. it is at the most exact possible degree at this time. */
  exact: boolean

  get exactIsSet(): boolean { return this.exact != null }

  /**
   * True if the planets are applying to the aspect, false otherwise.
   * Null-y if this.aspect is not present because the angle does not fall into any major orbs.
   */
  applying: boolean

  /** True if planets are moving farther apart from each other (shorter angle i.e. <= 180 is increasing), false otherwise. */
  planetsSeparating: boolean

  /** The angle between the two planets. */
  angle: number

  /** General information on the aspect. */
  aspect: Aspect

  /** Whether or not this planetary aspect falls within any of the aspects and their orbs. */
  get hasAspect(): boolean {
    return this.aspect != null
  }

  constructor(date: moment.Moment, planet1: PlanetaryPosition, planet2: PlanetaryPosition, exact?: boolean) {
    super(date)
    this.planet1 = planet1
    this.planet2 = planet2
    this.angle = PlanetaryAspect.getAngleBetween(planet1, planet2)
    this.aspect = Aspect.getAspectForAngleBinary(this.angle)
    this.exact = this.aspect && exact
    this.applying = this.aspect && (this.exact ? null : PlanetaryAspect.isApplyingToAspect(this.planet1.longitude, this.planet1.longitudeSpeed, this.planet2.longitude, this.planet2.longitudeSpeed, this.aspect.angle))
    this.planetsSeparating = PlanetaryAspect.planetsAreSeparating(this.planet1, this.planet2)
  }

  /** Calculate the angle between two longitudes, between 0 and 180. */
  static getAngleBetween(planet1: PlanetaryPosition, planet2: PlanetaryPosition): number {
    return Aspect.getAngleBetweenLongitudes(planet1.longitude, planet2.longitude)
  }

  /** Returns `true` if the planets are moving farther apart in the zodiac (i.e. increasing their angle in degrees), `false` otherwise. */
  static planetsAreSeparating(planet1: PlanetaryPosition, planet2: PlanetaryPosition): boolean {
    // the planets are separating if adding on longitudeSpeeds results in a larger angle, coming together otherwise.
    // e.g. [30, 0.005] means a planet at 30 degrees (0 Taurus) moving forward, [70, 0.001] means a planet at 70 degrees, moving forward.
    const daysToSeconds = 1 / (60 * 60 * 24)
    const difference = Aspect.getAngleBetweenLongitudes(planet1.longitude - planet1.longitudeSpeed * daysToSeconds, planet2.longitude - planet2.longitudeSpeed * daysToSeconds)
    const differenceAfter = Aspect.getAngleBetweenLongitudes(planet1.longitude, planet2.longitude)

    return differenceAfter > difference // separating if the angle difference is greater afterwards.
  }

  /** Returns `true` if the given longitudes and speeds represent planets that are moving closer to aspectAngle, and returns `false` if moving away from aspectAngle. */
  static isApplyingToAspect(longitude1: number, longitudeSpeed1: number, longitude2: number, longitudeSpeed2: number, aspectAngle: number): boolean {
    // Is the difference between planetary angle and aspect angle increasing (separating) or decreasing (applying)?
    const secondsPerDay = 24 * 60 * 60
    const difference = Math.abs(aspectAngle - Aspect.getAngleBetweenLongitudes(longitude1, longitude2))
    const differenceAfter = Math.abs(aspectAngle - Aspect.getAngleBetweenLongitudes(longitude1 + longitudeSpeed1 / secondsPerDay, longitude2 + longitudeSpeed2 / secondsPerDay))
    return differenceAfter < difference
  }

  /** Returns `true` if the angle is growing closer to aspectAngle, `false` otherwise. */
  static isApplyingToAspectFromAngles(angle: number, angleBefore: number, aspectAngle: number): boolean {
    const differenceBefore = Math.abs(aspectAngle - angleBefore)
    const differenceAfter = Math.abs(aspectAngle - angle)
    return differenceAfter < differenceBefore && !PlanetaryAspect.isCrossingOverAngle(angle, angleBefore, aspectAngle)
  }

  /** Returns the Aspect for the given planets (i.e. the angle between them). */  
  static getAspectBetween(planet1: PlanetaryPosition, planet2: PlanetaryPosition): Aspect {
    const angle = PlanetaryAspect.getAngleBetween(planet1, planet2)
    return Aspect.getAspectForAngle(angle)
  }

  /** Returns `true` if the two planets represented by the two longitudes are at a conjunction. */
  static isAtConjunction(longitude1: number, longitudeSpeed1: number, longitude2: number, longitudeSpeed2: number): boolean {
    const secondsPerDay = 24 * 60 * 60;
    const calculatePosition = (originalPosition: number, speed: number): number => originalPosition + speed / secondsPerDay;
    const angles = [ // before, now, after
      Aspect.getAngleBetweenLongitudes(calculatePosition(longitude1, -longitudeSpeed1), calculatePosition(longitude2, -longitudeSpeed2)),
      Aspect.getAngleBetweenLongitudes(longitude1, longitude2),
      Aspect.getAngleBetweenLongitudes(calculatePosition(longitude1, longitudeSpeed1), calculatePosition(longitude2, longitudeSpeed2))
    ];
    
    return angles[0] > angles[1] && angles[1] < angles[2]; // before > now, and now < after (now is in a valley in between before and after, and cannot get any lower).
  }

  /** Returns `true` if `angle1` and `angle2` straddle referenceAngle. */
  static isCrossingOverAngle(angle1: number, angle2: number, referenceAngle: number): boolean {
    // all other cases
    return (angle1 == referenceAngle && angle2 != referenceAngle) || (angle2 == referenceAngle && angle1 != referenceAngle) ||
          (angle1 < referenceAngle && angle2 > referenceAngle) || (angle1 > referenceAngle && angle2 < referenceAngle)
  }

}

/** Encapsulates a planetary phenomena, which is really the phase information of a planet, usually the moon. */
export class Phenomena extends PlanetaryEphemerisData {
  /** The date of the phenomena. */
  date: moment.Moment

  /** The planet. */
  planet: string

  /** The phase angle, from 0 to 180 degrees. */
  phaseAngle: number

  /** True if the illuminated disc is increasing, false otherwise. */
  waxing: boolean

  /** The percentage of the disc that is illuminated. */
  illumination: number

  /** The phase value, or illuminated percentage, from 0 to 1. */
  phase: Phase

  constructor(date: moment.Moment, data: any) {
    super(date)

    this.planet = data.planet
    this.phaseAngle = data.phaseAngle
    this.waxing = data.waxing
    this.illumination = data.phase
    this.phase = Phase.getPhase(Phase.getFullAngle(this.phaseAngle, this.waxing))
  }
}

/** Represents a house with given number, cusp, and end degree. */
export class HousePosition extends PlanetaryEphemerisData {
  /** The number of the house, 1-12. */
  number: number

  /** The index of the house 0-11. */
  get index() { return this.number - 1 }

  /** The cusp of the house, i.e. longitude. */
  cusp: number
  /** The end degree of the house, i.e. the longitude of the beginning of the following house. */
  end: number

  constructor(date: moment.Moment, number: number, cusp: number, endDegree: number) {
    super(date)
    this.number = number
    this.cusp = cusp
    this.end = endDegree
  }
}

/** Retrograde status for a given planet and date. */
export class RetrogradePosition extends PlanetaryEphemerisData {

  /** True if retrograde, false if direct. */
  retrograde: boolean

  /** True if direct, false if retrograde. */
  get direct() { return !this.retrograde }

  /** The planet for which the retrograde status was calculated. */
  planet: string

  constructor(date: moment.Moment, planet: string, retrograde: boolean) {
    super(date)
    this.planet = planet
    this.retrograde = retrograde
  }
}

/** A change in sign, retrograde, phase, or aspect status at a given date. */
export class ChangeEvent {
  /** Event change types. */
  static types = { sign: 'sign', retrograde: 'retrograde', aspect: 'aspect', phase: 'phase', moon: 'moon', natal: 'natal' }

  /** The date at which the change occurred. */
  date: moment.Moment

  /** The event type (see ChangeEvent.types). */
  type: string

  constructor(date: moment.Moment, type: string) {
    type = ChangeEvent.types[type]
    if (!type) {
      console.error('Invalid ChangeEvent type: ' + type)
      return
    }
    this.date = date
    this.type = type
  }

  get data() {
    return { date: this.date.valueOf(), type: this.type }
  }
}

export class SignChangeEvent extends ChangeEvent {

  planet: PlanetaryPosition
  previousPlanet: PlanetaryPosition

  constructor(date: moment.Moment, planet: PlanetaryPosition, previousPlanet: PlanetaryPosition) {
    super(date, ChangeEvent.types.sign)

    this.planet = planet
    this.previousPlanet = previousPlanet
  }
}

export class AspectChangeEvent extends ChangeEvent {
  aspect: PlanetaryAspect

  constructor(date: moment.Moment, aspect: PlanetaryAspect) {
    super(date, ChangeEvent.types.aspect)

    this.aspect = aspect
  }

}

export class NatalTransitEvent extends ChangeEvent {
  natalHouse: number
  natalPlanet: PlanetaryPosition
  transitingPlanet: PlanetaryPosition

  constructor(
    date: moment.Moment,
    public type: 'house' | 'aspect',
    public birthDate: number,
    public latitude: number,
    public longitude: number,
    data: any
  ) {
    super(date, ChangeEvent.types.natal)

    this.natalHouse = data.natalHouse;
    this.natalPlanet = data.natalPlanet;
    this.transitingPlanet = data.transitingPlanet;
  }
}

export class RetrogradeChangeEvent extends ChangeEvent {
  planet: PlanetaryPosition
  
  get retrograde(): boolean { return this.planet.retrograde }

  constructor(date: moment.Moment, planet: PlanetaryPosition) {
    super(date, ChangeEvent.types.retrograde)

    this.planet = planet
  }
}

export class PhaseChangeEvent extends ChangeEvent {
  phase: MoonPhase

  constructor(date: moment.Moment, phase: MoonPhase) {
    super(date, ChangeEvent.types.phase)
    this.phase = phase
  }
}

export class MoonChangeEvent extends ChangeEvent {
  /** The position of the moon. */
  moon: PlanetaryPosition

  get types(): string[] {
    return ['apogee', 'perigee', 'an', 'dn'].filter(f => this[f])
  }

  /** The moon is at its farthest distance away from Earth in its own orbit. */
  apogee: boolean

  /** The moon is at its closest place to the Earth in its own orbit. */
  perigee: boolean

  /** Ascending Node: The moon is crossing from below to above the ecliptic. */
  an: boolean

  /** Descending Node: The moon is crossing from above to below the ecliptic. */
  dn: boolean

  constructor(date: moment.Moment, moon: PlanetaryPosition, perigee: boolean, apogee: boolean, an: boolean, dn: boolean) {
    super(date, ChangeEvent.types.moon)
    this.moon = moon
    this.perigee = perigee
    this.apogee = apogee
    this.an = an
    this.dn = dn
  }
}
