
import * as mongoose from 'mongoose'
import * as moment from 'moment'

import env from '../env';
const databaseURI = env.MONGODB_URI;
(mongoose as any).Promise = global.Promise

import {
  AspectChange, RetrogradeChange, SignChange, PhaseChange, MoonChange, NatalChange,
  AspectEventDocument, PhaseEventDocument, RetrogradeEventDocument, MoonChangeDocument, NatalChangeDocument,
  SignEventDocument, EventDocument
} from '../schema/event.schema'

import { AspectChangeEvent, RetrogradeChangeEvent, SignChangeEvent, PhaseChangeEvent, NatalTransitEvent, MoonChangeEvent } from './astrology'

export const coordinatePlaces = 6;
export const roundCoordinate = (coord: number, places: number = coordinatePlaces) => Number(coord.toFixed(places));

/** Constants for each table. */
export const schemaTypes = ['aspect', 'retrograde', 'moon', 'phase', 'sign']

/** Retrieve the schema associated with the given type, for querying MongoDB. */
const getSchemaForType = (type: string): mongoose.Model<mongoose.Document> => {
  const schemaMap = {
    aspect: AspectChange,
    retrograde: RetrogradeChange,
    moon: MoonChange,
    phase: PhaseChange,
    sign: SignChange,
    natal: NatalChange
  }

  const match = schemaMap[type]
  if (!match) {
    throw new Error(`No schema found for type: ${type}.`);
  }

  return match
}

/** An interface for adding, updating and retrieving records with the database. */
export class EphemerisDatabase {

  /** Connect to the database. */
  connect() {
    return mongoose.connect(databaseURI, { reconnectTries: 2 })
      .then(() => console.log('connected to mongodb'))
      .catch(err => {
        console.error('error connecting to mongodb.', err)
      });
  }

  /** Gets the maximum date for all tables. */
  async getLatestDate(): Promise<{ [table: string]: moment.Moment }> {

    const getAggregateQuery = async (model: mongoose.Model<mongoose.Document>): Promise<moment.Moment> => {
      const result = await model.aggregate([
        { $sort: { date: -1 } },
        { $group: { _id: 'all', maxDate: { "$first": "$date" } } }
      ])
        .exec();

      return moment.utc(result.maxDate);
    };

    const tables = ['aspect', 'retrograde', 'phase', 'sign', 'moon']
    const queries = [AspectChange, RetrogradeChange, PhaseChange, SignChange, MoonChange]
      .map(schema => getAggregateQuery(schema))

    const dates = await Promise.all(queries);
    const result: { [table: string]: moment.Moment } = {}
    tables.forEach((table, i) => result[table] = dates[i])
    return result
  }

  /** Gets a change by mongo ID. */
  async getChangeById(eventId: string, type: string): Promise<EventDocument> {
    const schema = getSchemaForType(type)
    return await schema.findById(eventId).lean().exec();
  }

  /**
   * Get related planetary events arising before and after the given event.
   */
  async getSequentialChanges(fromEventId: string, type: string, beforeCount: number, afterCount: number): Promise<EventDocument[]> {
    const schema = getSchemaForType(type)

    // build the query criteria
    const selectPlanetField = type === 'aspect' ? 'planets' : (type === 'natal' ? 'natalPlanet transitingPlanet birthDate latitude longitude' : 'planet')
    const result = await schema.findOne({ _id: fromEventId }).select('_id date ' + selectPlanetField).lean().exec()
    const { date, _id, planet, planets, natalPlanet, transitingPlanet, latitude, longitude, birthDate } = {
      date: result.date,
      _id: result._id,
      planets: result.planets,
      planet: result.planet,
      natalPlanet: result.natalPlanet && result.natalPlanet.name,
      transitingPlanet: result.transitingPlanet && result.transitingPlanet.name,
      latitude: result.latitude,
      longitude: result.longitude,
      birthDate: result.birthDate
    };
    const criteria: any = { date: { $lte: date } }
    if (planet) {
      criteria['planet'] = planet;
    } else if (planets) {
      const or = [
        { 'planets.0.name': planets[0].name, 'planets.1.name': planets[1].name },
        { 'planets.1.name': planets[0].name, 'planets.0.name': planets[1].name }
      ];
      criteria['$or'] = or;
      if (type === 'aspect') {
        criteria['exact'] = true
      }
    } else if (transitingPlanet || natalPlanet) {
      criteria['birthDate'] = birthDate
      criteria['latitude'] = latitude
      criteria['longitude'] = longitude
      transitingPlanet && (criteria['transitingPlanet.name'] = transitingPlanet)
      natalPlanet && (criteria['natalPlanet.name'] = natalPlanet)
    }

    // search backward with the same criteria for related events in the past
    const backward = await schema
      .find(criteria)
      .sort({ date: -1 })
      .limit(beforeCount + 1)
      .select('_id date')
      .lean()
      .exec();
    const backwardDate = backward.length > 0 ? backward[backward.length - 1].date : null
    const newCriteria = { ...criteria, date: { $gte: backwardDate } }
    const finalResult = await schema
      .find(newCriteria)
      .sort({ date: 1 })
      .limit(beforeCount + afterCount + 1) // an even number of events on either side of the reference event
      .lean().exec();

    return finalResult || null;
  }

  async getNatalChanges(birthTimestamp: number, latitude: number, longitude: number, fromDate: moment.Moment, toDate: moment.Moment): Promise<NatalChangeDocument[]> {
    const dateCriteria = { $gte: fromDate.toDate(), $lt: toDate.toDate() }

    return await NatalChange
      .find({ date: dateCriteria, latitude: roundCoordinate(latitude), longitude: roundCoordinate(longitude), birthDate: birthTimestamp })
      .sort({ date: 1 })
      .lean()
      .exec()
  }

  /**
   * Gets a list of changes for each table, for the given range of time.
   * Populates EventDocument.averageFrequency for all events, and AspectEventDocument.retrogrades and AspectEventDocument.series.
   */
  async getChanges(fromDate: moment.Moment, toDate: moment.Moment, type?: string): Promise<EventDocument[]> {
    let date = { $gte: fromDate.toDate(), $lt: toDate.toDate() }

    const types = type ? [type] : schemaTypes;

    const queries = types.map(type => getSchemaForType(type).find({ date }).sort({ date: 1 }).lean())

    // flatten and sort the results by date
    const results = await Promise.all(queries)
    const result: EventDocument[] = [];
    results.forEach(r => result.push(...r));
    result.sort((a, b) => a.date.valueOf() > b.date.valueOf() ? 1 : -1);

    return result;
  }

  /** Save a moon event. */
  async saveMoonEvent(event: MoonChangeEvent): Promise<MoonChangeDocument[]> {
    return await Promise.all(event.types.map(async (type: string) => {
      const doc = new MoonChange({
        type: 'moon',
        date: event.date.toDate(),
        event: type,
        longitude: event.moon.longitude,
        latitude: event.moon.latitude,
        distance: event.moon.distance
      })

      return await doc.save();
    }));
  }

  /** Save a moon phase event. */
  async savePhaseEvent(event: PhaseChangeEvent): Promise<PhaseEventDocument> {
    return await new PhaseChange({
      type: 'phase',
      date: event.date.toDate(),
      planet: event.phase.planet,
      longitude: event.phase.longitude,
      distance: event.phase.distance,
      sunLongitude: event.phase.sunLongitude,

      phase: event.phase.phase.name,
      angle: event.phase.angle,
      waxing: event.phase.waxing
    })

  }

  /** Save a sign change event. */
  async saveSignEvent(event: SignChangeEvent): Promise<SignEventDocument> {
    return await new SignChange({
      type: 'sign',
      date: event.date.toDate(),
      planet: event.planet.planet,
      sign: event.planet.sign.name,
      longitude: event.planet.longitude,
      retrograde: event.planet.retrograde
    }).save();
  }

  /** Save a retrograde event. */
  async saveRetrogradeEvent(event: RetrogradeChangeEvent): Promise<RetrogradeEventDocument> {
    return await new RetrogradeChange({
      type: 'retrograde',
      date: event.date.toDate(),
      planet: event.planet.planet,
      retrograde: event.retrograde,
      longitude: event.planet.longitude
    }).save();
  }

  /** Save an aspect event. */
  async saveAspectEvent(event: AspectChangeEvent): Promise<AspectEventDocument> {
    return await new AspectChange({
      type: 'aspect',
      date: event.date.toDate(),
      planets: [event.aspect.planet1, event.aspect.planet2]
        .map(p => ({ name: p.planet, longitude: p.longitude, retrograde: p.retrograde })),
      aspect: event.aspect.aspect.name,
      major: event.aspect.aspect.major,
      applying: event.aspect.applying,
      planetsSeparating: event.aspect.planetsSeparating,
      exact: event.aspect.exact
    }).save();
  }

  /** Save a natal event or return the existing event for the given date and location. */
  async saveNatalEvent(event: NatalTransitEvent): Promise<NatalChangeDocument> {
    const latitude = roundCoordinate(event.latitude);
    const longitude = roundCoordinate(event.longitude);

    const existingNatal = await NatalChange.findOne({
      event: event.type,
      birthDate: event.birthDate,
      latitude,
      longitude,
      date: event.date.toDate()
    }).exec();

    if (existingNatal) {
      console.log('existing natal event: ', JSON.stringify(existingNatal.toJSON()))
    }

    return existingNatal || (await new NatalChange({
      type: 'natal',
      birthDate: event.birthDate,
      latitude,
      longitude,
      date: event.date.toDate(),
      event: event.type,
      natalPlanet: event.natalPlanet ? { name: event.natalPlanet.planet, longitude: event.natalPlanet.longitude, retrograde: event.natalPlanet.retrograde } : null,
      transitingPlanet: event.transitingPlanet ? { name: event.transitingPlanet.planet, longitude: event.transitingPlanet.longitude, retrograde: event.transitingPlanet.retrograde } : null,
      natalHouse: event.natalHouse
    }).save());
  }
}

export const ephemerisDatabase = new EphemerisDatabase();
ephemerisDatabase.connect();
