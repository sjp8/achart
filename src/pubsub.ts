//// Test message
// gcloud beta pubsub topics publish populate_natal_background --message '{"startDate":"2018-05-01T00:00:00.000Z","endDate":"2018-06-01T00:00:00.000Z","birthDate":"1988-02-03T19:46:00.000Z","longitude":-118.2157933,"latitude":34.2048286}'

import * as moment from 'moment';

import { EphemerisDatabase } from './logic/ephemerisDatabase';
import { ephemeris as eph } from './logic/astrology/ephemeris';

export interface PopulateNatalRequest {
  latitude: number;
  longitude: number;
  birthDate: string;
  startDate: string;
  endDate: string;
}

/**
 * Background Cloud worker for processing natal population requests.
 */
export async function populate_natal_background(event): Promise<any> {
  const ephemerisDatabase = new EphemerisDatabase();
  try {
    await ephemerisDatabase.connect();
  } catch (err) {
    return Promise.reject(err);
  }

  return new Promise((resolve, reject) => {
    const message = event.data;
    const data: PopulateNatalRequest = message.data ? JSON.parse(Buffer.from(message.data, 'base64').toString()) : null;
    if (data) {
      const birthDate = moment(data.birthDate);
      const startDate = moment(data.startDate);
      const endDate = moment(data.endDate);
      const latitude = Number(data.latitude);
      const longitude = Number(data.longitude);

      if ([birthDate, startDate, endDate].some(date => !date.isValid())) {
        console.error('invalid birth, start, or end date.');
        return reject('date format');
      }

      if ([latitude, longitude].some(coord => isNaN(coord))) {
        console.error('invalid latitude or longitude. Must provide valid numerical values.');
        return reject('coordinate format');
      }

      console.log('searching for natal changes with the following args: ' + JSON.stringify(data) + '.');

      const natalPositions = eph.getPlanetsArray(birthDate);
      const natalHouses = eph.getHouses(birthDate, longitude, latitude);
      eph.searchNatalChanges(birthDate.unix(), latitude, longitude, natalPositions, natalHouses, startDate, endDate,
        async change => {
          console.log('change found, saving. ' + change.transitingPlanet.planet + ' type ' + change.type);
          try {
            const doc = await ephemerisDatabase.saveNatalEvent(change)
            console.log('change saved ', doc._id);
          } catch (ex) {
            console.error('error saving natal event for ' + change.birthDate + ', ' + change.latitude + ', ' + change.longitude + '.')
            console.error(ex)
          }
        },
        () => {
          console.log('search natal changes done')
          setTimeout(async () => {
            console.log('exiting function')
            resolve();
          }, 10000);
        }
      )
    }
  });
}
