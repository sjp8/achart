
import * as express from 'express'
import * as moment from 'moment'
import * as compression from 'compression'

export const app = express()

// CORS: allow any origin
app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*')
  next()
})

// Request Logging with duration
app.use((req, res, next) => {
  const startTime = moment()
  next()
  const duration = moment().diff(startTime, 'ms')
  console.log(`Request: ${req.method} (${duration}ms), IP: ${req.ip}, URL: ${req.url}`)
})

// Use gzip compression
app.use(compression())
