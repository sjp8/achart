
import handlers from '../handlers'
import * as express from 'express'

/** Add routes to the express app. See ../index.ts for handler descriptions. */
export const addRoutes = (app: express.Application) => {
  app.get('/', handlers.informationHandler)
  app.get('/ephemeris', handlers.ephemerisHandler)
  app.get('/sequential_events/:eventId/:type', handlers.sequentialEventsHandler)
  app.get('/sequential_events', handlers.sequentialEventsHandler)
  app.get('/natal_events', handlers.natalEventsHandler)
  app.get('/event/:type/:eventId', handlers.eventHandler)
  app.get('/event', handlers.eventHandler)
  app.get('/events', handlers.eventsHandler)
  app.get('/events/:type', handlers.eventsHandler)
  app.get('/zodiac', handlers.zodiacHandler)
  app.get('/populate_natal', handlers.populateNatalHandler)
}
