
import { Sign, Body, Aspect } from '../logic/astrology'

import * as mongoose from 'mongoose'

const signs = Sign.signs.map(sign => sign.name)
const planets = Body.bodies.map(body => body.name)
const aspects = Aspect.aspects.map(aspect => aspect.name)

const ObjectId = mongoose.Schema.Types.ObjectId

export interface EventDocument extends mongoose.Document {
  /** The mongoDB id */
  _id: string

  /** The date of the event. */
  date: Date

  /** Information on the average frequency of this event. */
  frequency: { minimum: number, maximum: number, mean: number, count: number }

  type: string
}

const mixinDefaults = (schema: any): any => {
  schema.type = String
  schema.date = Date
  schema.frequency = {
    minimum: Number, maximum: Number, mean: Number, count: Number
  }
  return schema
}


export const aspectChangeSchema = new mongoose.Schema(mixinDefaults({
  planets: [{
    name: String,
    longitude: Number,
    retrograde: Boolean
  }],
  aspect: String,
  major: Boolean,
  applying: Boolean,
  exact: Boolean,
  planetsSeparating: Boolean,
  inferior: Boolean,
}))

aspectChangeSchema.index({ date: 1 })
aspectChangeSchema.index({ aspect: 1 })
aspectChangeSchema.index({ major: 1 })
aspectChangeSchema.index({ exact: 1 })

export interface AspectEventDocument extends EventDocument {
  planets: {
    name: string,
    longitude: number,
    retrograde: boolean
  }[]
  aspect: string
  major: boolean
  applying: boolean
  exact: boolean
  planetsSeparating: boolean
  inferior: boolean
}
export const AspectChange = mongoose.model<AspectEventDocument>('AspectChange', aspectChangeSchema)


export const signChangeSchema = new mongoose.Schema(mixinDefaults({
  planet: { type: String, enum: planets },
  sign: String,
  longitude: Number,
  retrograde: Boolean
}))

signChangeSchema.index({ date: 1 })
signChangeSchema.index({ planet: 1 })

export interface SignEventDocument extends EventDocument {
  planet: string
  sign: string
  longitude: number
  retrograde: boolean
}

export const SignChange = mongoose.model<SignEventDocument>('SignChange', signChangeSchema)

export const retrogradeChangeSchema = new mongoose.Schema(mixinDefaults({
  planet: String,
  retrograde: Boolean,
  longitude: Number,
}))

retrogradeChangeSchema.index({ date: 1 })
retrogradeChangeSchema.index({ planet: 1 })

export interface RetrogradeEventDocument extends EventDocument {
  planet: string
  retrograde: boolean
  longitude: number
}

export const RetrogradeChange = mongoose.model<RetrogradeEventDocument>('RetrogradeChange', retrogradeChangeSchema)


export const phaseChangeSchema = new mongoose.Schema(mixinDefaults({
  planet: String,
  longitude: Number,
  sunLongitude: Number,
  distance: Number,

  phase: String,
  waxing: Boolean,
  angle: Number,
  percent: Number
}))

phaseChangeSchema.index({ date: 1 })
phaseChangeSchema.index({ phase: 1 })

export interface PhaseEventDocument extends EventDocument {
  planet: string
  longitude: number
  sunLongitude: number
  distance: number

  phase: string
  waxing: boolean
  angle: number
  percent: number
}

export const PhaseChange = mongoose.model<PhaseEventDocument>('PhaseChange', phaseChangeSchema)

export const moonChangeSchema = new mongoose.Schema(mixinDefaults({
  event: { type: String, enum: ['perigee', 'apogee', 'voc', 'an', 'dn'] },
  longitude: Number,
  latitude: Number,
  distance: Number
}))

moonChangeSchema.index({ date: 1 })
moonChangeSchema.index({ type: 1 })

export interface MoonChangeDocument extends EventDocument {
  event: string
  longitude: number
  latitude: number
  distance: number
}

export const MoonChange = mongoose.model<MoonChangeDocument>('MoonChange', moonChangeSchema)

export const natalChangeSchema = new mongoose.Schema(mixinDefaults({
  birthDate: Number,
  latitude: Number,
  longitude: Number,
  event: { type: String, enum: ['house', 'aspect'] },
  natalPlanet: { name: String, longitude: Number, retrograde: Boolean },
  natalHouse: Number,
  transitingPlanet: { name: String, longitude: Number, retrograde: Boolean }
}))
natalChangeSchema.index({ date: 1 })
natalChangeSchema.index({ latitude: 1, longitude: 1 })
natalChangeSchema.index({ birthDate: 1 })

export interface NatalChangeDocument extends EventDocument {
  event: 'house' | 'aspect'
  birthDate: number
  latitude: number
  longitude: number
  natalPlanet?: { name: string, longitude: number, retrograde: boolean }
  natalHouse?: number
  transitingPlanet: { name: string, longitude: number, retrograde: boolean }
}

export const NatalChange = mongoose.model<NatalChangeDocument>('NatalChange', natalChangeSchema)
